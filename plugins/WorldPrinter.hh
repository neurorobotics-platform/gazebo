#ifndef _GAZEBO_WORLD_PRINTER_PLUGIN_
#define _GAZEBO_WORLD_PRINTER_PLUGIN_

#include <gazebo/gazebo.hh>

namespace gazebo
{
  class GAZEBO_VISIBLE WorldPrinter : public WorldPlugin
  {
    public: WorldPrinter();
    public: void Load(physics::WorldPtr _world, sdf::ElementPtr _sdf);
    public: void Init();
    private: void OnWorldUpdate();
    private: event::ConnectionPtr updateConnection;
    private: physics::WorldPtr world;
  };
  
  GZ_REGISTER_WORLD_PLUGIN(WorldPrinter)
} 


#endif