
#include "WorldPrinter.hh"
#include <gazebo/physics/World.hh>
#include <gazebo/physics/Model.hh>
#include <gazebo/physics/Link.hh>
#include "gazebo/common/Console.hh"

#pragma GCC diagnostic ignored "-Wunused-parameter"

using namespace gazebo;
 
WorldPrinter::WorldPrinter()
{

}

void WorldPrinter::Load(physics::WorldPtr _world, sdf::ElementPtr _sdf)
{

  gzmsg << "----------- World Entity Tree --------------" << std::endl;
  _world->PrintEntityTree();
  gzmsg << "--------------------------------------------" << std::endl;
  this->world = _world;
}


void WorldPrinter::Init()
{
  gazebo::WorldPlugin::Init();
  this->updateConnection = event::Events::ConnectWorldUpdateBegin(
          boost::bind(&WorldPrinter::OnWorldUpdate, this));
}


void WorldPrinter::OnWorldUpdate()
{
  gzmsg << "Step t = " << this->world->SimTime().Double() << std::endl;
  const auto &models = this->world->Models();
  for (physics::ModelPtr model : models)
  {    
    const auto &links = model->GetLinks();
    for (physics::LinkPtr link : links)
    {
      ignition::math::Pose3d wpose = link->WorldPose();
      gzmsg << link->GetScopedName() << " WP=" << wpose.Pos() << " WR=" << wpose.Rot().Euler() << std::endl;
    }
  }
}

  