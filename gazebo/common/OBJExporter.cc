#include "gazebo/common/Material.hh"
#include "gazebo/common/Mesh.hh"
#include "gazebo/common/Console.hh"

#include "gazebo/common/SystemPaths.hh"

#include "gazebo/common/OBJExporter.hh"

using namespace gazebo;
using namespace common;

OBJExporter::OBJExporter():
  MeshExporter(), extra_model_transform(ignition::math::Matrix4d::Identity)
{

}

OBJExporter::~OBJExporter()
{

}

void OBJExporter::Export(const Mesh *_mesh, const std::string &_filename,
    bool)
{
  std::string obj_file_base_path;

  boost::filesystem::path objFilePath(_filename);
  if (!objFilePath.is_absolute())
  {
    obj_file_base_path = gazebo::common::SystemPaths::Instance()->TmpInstancePath();
    obj_file_base_path += "/";
    obj_file_base_path += _filename;
  }
  else
  {
    obj_file_base_path = _filename;
  }

  std::ofstream os;
  os.open(obj_file_base_path, std::ios::trunc | std::ios::out);

  if (os.is_open())
  {
    for (unsigned int k = 0; k < _mesh->GetSubMeshCount(); ++k)
    {
      const common::SubMesh *subMesh = _mesh->GetSubMesh(k);
      if (subMesh->GetPrimitiveType() == common::SubMesh::TRIANGLES)
      {
        ignition::math::Matrix4d scale_matrix = ignition::math::Matrix4d::Identity;
        scale_matrix.Scale(subMesh->originalScaleFactor);
        ignition::math::Matrix4d final_trafo = extra_model_transform * scale_matrix;

        std::vector<ignition::math::Vector3d> vertices = subMesh->GetOriginalVertices();
        for (unsigned int i = 0; i < vertices.size(); i++)
        {
          os << "v ";
          ignition::math::Vector3d v = final_trafo * vertices[i];
          os << v << "\n";
        }

        std::vector<unsigned int> indices = subMesh->GetOriginalIndices();
        assert(indices.size() % 3 == 0);
        for (unsigned int i = 0; i < indices.size(); i += 3)
        {
          os << "f ";
          os << indices[i  ] + 1 << " ";
          os << indices[i+1] + 1 << " ";
          os << indices[i+2] + 1 << "\n";
        }
      }
      else
      {
        gzwarn << "OBJExporter encounted submesh of unsupported primitives. Only triangles will be exported." << std::endl;
      }
    }

    os.close();
  }
}