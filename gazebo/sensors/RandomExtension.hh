/*
 * Copyright (C) 2012 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#ifndef RANDOM_EXTENSION_HH_
#define RANDOM_EXTENSION_HH_

#include <random>
#include <cmath>
#include <cstdint>

namespace ignition
{
  namespace math
  {
      //
      /// \def GeneratorType
      typedef std::mt19937 GeneratorType;
      /// \brief std::gamma_distribution
      typedef std::gamma_distribution<double> GammaRealDist;
      /// \brief std::cauchy_distribution<double>
      typedef std::cauchy_distribution<double> CauchyRealDist;
      /// \def LognormalDist
      /// \brief std::uniform_int<double>
      typedef std::lognormal_distribution<double> LognormalRealDist;

      /// \def FisherRealDist
      /// \brief fisher_f_distribution<double>
      typedef std::fisher_f_distribution<double> FisherRealDist;

      /// \def SquaredRealDist
      /// \brief std::chi_squared_distribution<double>
      typedef std::chi_squared_distribution<double> SquaredRealDist;

      /// \class Rand Rand.hh ignition/math/Rand.hh
      /// \brief Random number generator class
      class RandomExtension
      {

          /// \brief Set the seed value.
        /// \param[in] _seed The seed used to initialize the randon number
        /// generator.
        public: static void Seed(unsigned int _seed);

        /// \brief Get the seed value.
        /// \return The seed value used to initialize the random number
        /// generator.
        public: static unsigned int Seed();

        public: static double DblGamma(double _mean = 0, double _sigma = 1);
        public: static double DblCauchy(double _mean = 0, double _sigma = 1);
        public: static double DblLognormal(double _mean = 4, double _sigma = 4);
        public: static double DblFisher(double _mean = 2.0,double _sigma = 2.0 );
        public: static double DblSquared(double _mean = 1);

        /// \brief Get a mutable reference to the seed (create the static
        /// member if it hasn't been created yet).
        private: static uint32_t &SeedMutable();

        /// \brief Get a mutable reference to the random generator (create the
        /// static member if it hasn't been created yet).
        private: static GeneratorType &RandGenerator();
      };
  }
}
#endif
