/***************************************************************************

This source file is part of OGREBULLET
(Object-oriented Graphics Rendering Engine Bullet Wrapper)
For the latest info, see http://www.ogre3d.org/phpBB2addons/viewforum.php?f=10

Copyright (c) 2007 tuan.kuranes@gmail.com (Use it Freely, even Statically, but have to contribute any changes)



Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
#ifndef _OgreBulletCollisions_DebugDrawer_H_
#define _OgreBulletCollisions_DebugDrawer_H_

#include <OGRE/Ogre.h>


#include "OgreDebugLines.hh"

namespace gazebo
{
  namespace physics
  {
    enum DebugDrawModes
    {
      DBG_NoDebug=0,
      DBG_DrawWireframe = 1,
      DBG_DrawAabb=2,
      DBG_DrawFeaturesText=4,
      DBG_DrawContactPoints=8,
      DBG_NoDeactivation=16,
      DBG_NoHelpText = 32,
      DBG_DrawText=64,
      DBG_ProfileTimings = 128,
      DBG_DrawConstraints = (1 << 11),
      DBG_DrawConstraintLimits = (1 << 12),
      DBG_FastWireframe = (1<<13),
      DBG_DrawNormals = (1<<14),
      DBG_DrawFrames = (1<<15),
      DBG_MAX_DEBUG_DRAW_MODE
    };

    //------------------------------------------------------------------------------------------------
    class DebugDrawer: public DebugLines, public Ogre::FrameListener
    {
    public:
      DebugDrawer(Ogre::SceneManager* scm);
      virtual ~DebugDrawer();

      virtual void setDebugMode(int mode) { mDebugMode = mode; }
      virtual int getDebugMode() const { return mDebugMode; }

      virtual void drawLine(const Ogre::Vector3 &from, const Ogre::Vector3 &to, const Ogre::Vector3 &color);
      virtual void drawContactPoint(const Ogre::Vector3 &PointOnB, const Ogre::Vector3 &normalOnB,
                                    double distance, int lifeTime, const Ogre::Vector3 &color);

      virtual void drawTriangle(const Ogre::Vector3 & v0,const Ogre::Vector3& v1,const Ogre::Vector3 & v2,const Ogre::Vector3 & color, double /*alpha*/);

      void drawAabb(const Ogre::Vector3 &from, const Ogre::Vector3 &to, const Ogre::Vector3 &color);
      void drawContactPoint(const Ogre::Vector3 &PointOnB, const Ogre::Vector3 &normalOnB,
                            Ogre::Real distance, int lifeTime, const Ogre::Vector3 &color);

      void setDrawAabb(bool enable);
      void setDrawWireframe(bool enable);
      void setDrawFeaturesText(bool enable);
      void setDrawContactPoints(bool enable);
      void setNoDeactivation(bool enable);
      void setNoHelpText(bool enable);
      void setDrawText(bool enable);

      bool doesDrawAabb() const;
      bool doesDrawWireframe() const;
      bool doesDrawFeaturesText() const;
      bool doesDrawContactPoints() const;
      bool doesNoDeactivation() const;
      bool doesNoHelpText() const;
      bool doesDrawText() const;
      bool doesProfileTimings() const;

      void reportErrorWarning(const char *warningString);

    protected:
      int mDebugMode;

    protected:
      bool frameStarted(const Ogre::FrameEvent& evt);
      bool frameEnded(const Ogre::FrameEvent& evt);
    };
  }
}
#endif //_OgreBulletCollisions_DebugDrawer_H_

