/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#ifndef _OPENSIM_LINK_HH_
#define _OPENSIM_LINK_HH_

#include <vector>
#include <memory>

#include "gazebo/physics/opensim/OpensimTypes.hh"
#include "gazebo/physics/Link.hh"

#include "gazebo/physics/opensim/opensim_inc.h"
#include "gazebo/util/system.hh"

namespace gazebo
{
  namespace physics
  {
    /// \ingroup gazebo_physics
    /// \addtogroup gazebo_physics_opensim Opensim Physics
    /// \brief opensim physics engine wrapper
    /// \{

    /// \brief Opensim Link class
    class GZ_PHYSICS_VISIBLE OpensimLink : public Link
    {
      /// \brief Constructor
      public: OpensimLink(EntityPtr _parent, OpensimPhysics *_physics);

      /// \brief Destructor
      public: virtual ~OpensimLink() override;

      // Documentation inherited.
      public: virtual void Load(sdf::ElementPtr _ptr) override;

      // Documentation inherited.
      public: virtual void Init() override;

      // Documentation inherited.
      public: virtual void Fini() override;

      // Documentation inherited.
      public: virtual void Reset() override;

      // Documentation inherited.
      public: virtual void OnPoseChange() override;

      // Documentation inherited.
      public: virtual void SetEnabled(bool enable) const override;

      // Documentation inherited.
      public: virtual bool GetEnabled() const override;

      // Documentation inherited.
      public: virtual void SetLinearVel(const ignition::math::Vector3d &_vel) override;

      // Documentation inherited.
      public: virtual void SetAngularVel(const ignition::math::Vector3d &_vel) override;

      // Documentation inherited.
      public: virtual void SetForce(const ignition::math::Vector3d &_force) override;

      // Documentation inherited.
      public: virtual void SetTorque(const ignition::math::Vector3d &_force) override;

      // Documentation inherited.
      public: virtual ignition::math::Vector3d WorldLinearVel(
        const ignition::math::Vector3d& _vector3) const override;

      // Documentation inherited.
      public: virtual ignition::math::Vector3d WorldLinearVel(
          const ignition::math::Vector3d &_offset,
          const ignition::math::Quaterniond &_q) const override;

      // Documentation inherited.
      public: virtual ignition::math::Vector3d WorldCoGLinearVel() const override;

      // Documentation inherited.
      public: virtual ignition::math::Vector3d WorldAngularVel() const override;

      // Documentation inherited.
      public: virtual ignition::math::Vector3d WorldForce() const override;

      // Documentation inherited.
      public: virtual ignition::math::Vector3d WorldTorque() const override;

      // Documentation inherited.
      public: virtual void SetGravityMode(bool _mode) override;

      // Documentation inherited.
      public: virtual bool GetGravityMode() const override;

      // Documentation inherited.
      public: virtual void SetSelfCollide(bool _collide) override;

      // Documentation inherited.
      public: virtual void SetLinearDamping(double _damping) override;

      // Documentation inherited.
      public: virtual void SetAngularDamping(double _damping) override;

      // Documentation inherited.
      public: virtual void AddForce(const ignition::math::Vector3d &_force) override;

      // Documentation inherited.
      public: virtual void AddRelativeForce(const ignition::math::Vector3d &_force) override;

      // Documentation inherited.
      public: virtual void AddForceAtWorldPosition(const ignition::math::Vector3d &_force,
                                                   const ignition::math::Vector3d &_pos) override;

      // Documentation inherited.
      public: virtual void AddForceAtRelativePosition(
                  const ignition::math::Vector3d &_force,
                  const ignition::math::Vector3d &_relpos) override;

      // Documentation inherited
      public: virtual void AddLinkForce(const ignition::math::Vector3d &_force,
          const ignition::math::Vector3d &_offset = ignition::math::Vector3d::Zero) override;

      // Documentation inherited.
      public: virtual void AddTorque(const ignition::math::Vector3d &_torque) override;

      // Documentation inherited.
      public: virtual void AddRelativeTorque(const ignition::math::Vector3d &_torque) override;

      // Documentation inherited.
      public: virtual void SetAutoDisable(bool _disable) override;

      // Documentation inherited.
      public: virtual void UpdateMass() override;

      /// \brief If the inboard body of this link is ground, simply
      /// lock the inboard joint to freeze it to ground.  Otherwise,
      /// add a weld constraint to simulate freeze to ground effect.
      /// \param[in] _static if true, freeze link to ground.  Otherwise
      /// unfreeze link.
      public: virtual void SetLinkStatic(bool _static) override;

      /// \brief Internal call to set link static
      /// based on staticLink if staticLinkDirty is true.
      private: void ProcessSetLinkStatic();

      public: void SetDirtyPose(const ignition::math::Pose3d &_pose);

      /// \brief Internal call to change effect of gravity on Link
      /// based on gravityMode if gravityModeDirty is true.
      private: void ProcessSetGravityMode();

      public: friend class OpensimLinkPrivate;
      public: OpensimLinkPrivate* GetPriv() { return dataPtr.get(); }
      public: const OpensimLinkPrivate* GetPriv() const { return const_cast<OpensimLink*>(this)->GetPriv(); }

      private: boost::shared_ptr<OpensimLinkPrivate> dataPtr;
#if 0
      /// \brief store gravity mode given link might not be around
      private: bool gravityMode;

      /// \brief Trigger setting of link gravity mode
      private: bool gravityModeDirty;

      /// \brief Trigger setting of link according to staticLink.
      private: bool staticLinkDirty;

      /// \brief If true, freeze link to world (inertial) frame.
      private: bool staticLink;

      /// \brief Event connection for SetLinkStatic
      private: event::ConnectionPtr staticLinkConnection;

      /// \brief Event connection for SetGravityMode
      private: event::ConnectionPtr gravityModeConnection;

      /// \brief save opensim free state for reconstructing opensim model graph
      private: std::vector<double> opensimQ;

      /// \brief save opensim free state for reconstructing opensim model graph
      private: std::vector<double> opensimU;
#endif
    };
    /// \}
  }
}
#endif
