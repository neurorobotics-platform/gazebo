/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#include <string>

#include <thread>

#include "gazebo/physics/opensim/OpensimTypes.hh"
#include "gazebo/physics/opensim/OpensimModel.hh"
#include "gazebo/physics/opensim/OpensimLink.hh"
#include "gazebo/physics/opensim/OpensimJoint.hh"
#include "gazebo/physics/opensim/OpensimCollision.hh"

#include "gazebo/physics/opensim/OpensimPlaneShape.hh"
#include "gazebo/physics/opensim/OpensimSphereShape.hh"
#include "gazebo/physics/opensim/OpensimHeightmapShape.hh"
#include "gazebo/physics/opensim/OpensimMultiRayShape.hh"
#include "gazebo/physics/opensim/OpensimBoxShape.hh"
#include "gazebo/physics/opensim/OpensimCylinderShape.hh"
#include "gazebo/physics/opensim/OpensimMeshShape.hh"
#include "gazebo/physics/opensim/OpensimPolylineShape.hh"
#include "gazebo/physics/opensim/OpensimRayShape.hh"

#include "gazebo/physics/opensim/OpensimHingeJoint.hh"
#include "gazebo/physics/opensim/OpensimUniversalJoint.hh"
#include "gazebo/physics/opensim/OpensimBallJoint.hh"
#include "gazebo/physics/opensim/OpensimSliderJoint.hh"
#include "gazebo/physics/opensim/OpensimHinge2Joint.hh"
#include "gazebo/physics/opensim/OpensimScrewJoint.hh"
#include "gazebo/physics/opensim/OpensimFixedJoint.hh"

#include "gazebo/physics/opensim/OpensimMuscle.hh"

#include "gazebo/physics/ContactManager.hh"
#include "gazebo/physics/PhysicsTypes.hh"
#include "gazebo/physics/PhysicsFactory.hh"
#include "gazebo/physics/World.hh"
#include "gazebo/physics/WorldPrivate.hh"
#include "gazebo/physics/Entity.hh"
#include "gazebo/physics/Model.hh"
#include "gazebo/physics/SurfaceParams.hh"
#include "gazebo/physics/MapShape.hh"

#include "gazebo/common/Assert.hh"
#include "gazebo/common/Console.hh"
#include "gazebo/common/Exception.hh"
#include "gazebo/common/Error.hh"

#include "gazebo/transport/Publisher.hh"

#include "gazebo/physics/opensim/OpensimPhysics.hh"
#include "gazebo/physics/opensim/OpensimPhysicsPrivate.hh"

#include "gazebo/physics/OgreDebugDrawer.hh"


using namespace gazebo;
using namespace physics;
using SimTK::Vec3;
using SimTK::UnitVec3;
using SimTK::Rotation;


GZ_REGISTER_PHYSICS_ENGINE("opensim", OpensimPhysics)

/////////////////////////////////////////////////
static void SetMassProperties(OpenSim::Body &_b, const SimTK::MassProperties &_props)
{
  // Ok, so the supplied MassProperties is suitable for Simbody. It's inertia tensor
  // expressed in the local body frame about the body frame origin. However OpenSim
  // expects the inertia tensor in some other coordinates. We have to revert the
  // operation inertiaAboutCOM.shiftFromMassCenter(_massCenter, _mass); found in
  // OpenSim::Body::getMassProperties().
  SimTK::Inertia I = _props.getInertia();
  SimTK::Inertia inertiaAboutCom = I.shiftToMassCenter(_props.getMassCenter(), _props.getMass());
  _b.setInertia(inertiaAboutCom);
  _b.setMass(_props.getMass());
  _b.setMassCenter(_props.getMassCenter());
}

/////////////////////////////////////////////////
static std::string FormatName(Base *obj)
{
  return obj ? obj->GetName() : "NULL";
}

/////////////////////////////////////////////////
static std::string FormatName(OpenSim::Object *obj)
{
  return obj ? obj->getName() : "NULL";
}

/////////////////////////////////////////////////
static void Pose2PosAngles(Vec3 &t, Vec3 &angles, const ignition::math::Pose3d &_pose)
{
  SimTK::Transform trafo = Pose2Transform(_pose);
  Rotation r = trafo.R();
  angles = r.convertRotationToBodyFixedXYZ();
  t = trafo.p();
}

/////////////////////////////////////////////////
std::string prettyPrintTransform(const SimTK::Transform &X)
{
  std::stringstream os;
  const SimTK::Rotation R = X.R();
  const SimTK::Vec3 p = X.p();
  const SimTK::Rotation::Mat33P M = R.toMat33();
  constexpr int prec = 3;
  constexpr int w    = 8;
  os << std::fixed << std::setw( w ) << std::setprecision( prec ) << M(0,0)
     << std::fixed << std::setw( w ) << std::setprecision( prec ) << M(0,1)
     << std::fixed << std::setw( w ) << std::setprecision( prec ) << M(0,2)
     << std::fixed << std::setw( w ) << std::setprecision( prec ) << p[0] << std::endl;
  os << std::fixed << std::setw( w ) << std::setprecision( prec ) << M(1,0)
     << std::fixed << std::setw( w ) << std::setprecision( prec ) << M(1,1)
     << std::fixed << std::setw( w ) << std::setprecision( prec ) << M(1,2)
     << std::fixed << std::setw( w ) << std::setprecision( prec ) << p[1] << std::endl;
  os << std::fixed << std::setw( w ) << std::setprecision( prec ) << M(2,0)
     << std::fixed << std::setw( w ) << std::setprecision( prec ) << M(2,1)
     << std::fixed << std::setw( w ) << std::setprecision( prec ) << M(2,2)
     << std::fixed << std::setw( w ) << std::setprecision( prec ) << p[2];
  return os.str();
}

//////////////////////////////////////////////////
namespace
{

template<class Functor>
void ForTheConstModelHierarchy(const Model &_model, Functor _fun)
{
  _fun(_model);
  for (const ModelPtr child_ptr : _model.NestedModels())
  {
    ForTheConstModelHierarchy(*child_ptr, _fun);
  }
}

template<class Functor>
void ForTheModelHierarchy(Model &_model, Functor _fun)
{
  _fun(_model);
  for (ModelPtr child_ptr : _model.NestedModels())
  {
    ForTheModelHierarchy(*child_ptr, _fun);
  }
}

}

namespace gazebo { namespace physics {
namespace MultibodyGraphBuilding
{
SimTK::MultibodyGraphMaker CreateMultibodyGraph(const OpensimModel &_model);
}
}}

//////////////////////////////////////////////////
OpensimPhysics::OpensimPhysics(WorldPtr _world)
    : PhysicsEngine(_world)
     // The idea is to scale parameters so that the opensim
     // behaviour is similar to the default case of other
     // physics engines. But it fail in one of the unit
     // test where the stopping force is abused as relatively soft
     // spring damper system. Hence we set the scaling factors
     // to 1, and will perhaps remove them later on if they prove
     // useless with certainty.
      , stopStiffnessMultiplier(1.) //(1.e-4)
      , stopDissipationMultiplier(1.) //(1.e2)
      , stopWidthFraction(1.e-2)
      , dataPtr(new OpensimPhysicsPrivate(_world, this))
{
  OPENSIM_PHYSICS_LOG_CALL();
  #if !(ASSIGN_UNIQUE_OPENSIM_OBJECT_NAMES)
    gzwarn << "Unique name generation for OpenSim objects is disabled!" << std::endl;
  #endif

  this->node = transport::NodePtr(new transport::Node());
  this->node->Init(this->world->Name());
  // Todo: Name-dependent topic URL?
  // Adjust queue length and publishing frequency to link pose update message publisher.
  // To FIX: Muscle messages are send one per model in immediate succession. Rate limiter therefore eats muscle messages.
  this->muscleInfoPublisher =
    this->node->Advertise<msgs::OpenSimMuscles>("~/muscles", /*50*/ 10, 60);

  
  this->errorPublisher = this->node->Advertise<msgs::Error>("~/error");
  this->waitForConnectionOnError = true;

  this->opensimPhysicsInitialized = false;
  this->opensimPhysicsFirstStart = true;
}

//////////////////////////////////////////////////
OpensimPhysics::~OpensimPhysics()
{
  OPENSIM_PHYSICS_LOG_CALL();
  // TODO: investigate why this is not called
}

//////////////////////////////////////////////////
void OpensimPhysics::Load(sdf::ElementPtr _sdf)
{
  OPENSIM_PHYSICS_LOG_CALL();
  PhysicsEngine::Load(_sdf);

  dataPtr->Load(_sdf);

  sdf::ElementPtr opensimElem = this->sdf->GetElement("opensim");
  if (opensimElem == nullptr)
    gzthrow("Cannot get the opensim element from the sdf.");

  // Set stiction max slip velocity to make it less stiff.
  // DON'T FORGET TO DO SOMETHING ABOUT THIS
  //this->contact.setTransitionVelocity(
  //  opensimElem->Get<double>("max_transient_velocity"));

  sdf::ElementPtr opensimContactElem = opensimElem->GetElement("contact");
  if (opensimContactElem == nullptr)
    gzthrow("Cannot get the opensim contact element from the sdf.");

  this->globalOverrideSurfaceParams.stiffness =
    opensimContactElem->Get<double>("stiffness");
  this->globalOverrideSurfaceParams.dissipation =
    opensimContactElem->Get<double>("dissipation");
  this->globalOverrideSurfaceParams.staticFriction =
    opensimContactElem->Get<double>("static_friction");
  this->globalOverrideSurfaceParams.dynamicFriction =
    opensimContactElem->Get<double>("dynamic_friction");
  this->globalOverrideSurfaceParams.viscousFriction =
    opensimContactElem->Get<double>("viscous_friction");
  this->globalOverrideSurfaceParams.transitionVelocity =
    opensimContactElem->Get<double>("stiction_transition_velocity");
}

/////////////////////////////////////////////////
void OpensimPhysics::OnRequest(ConstRequestPtr &_msg)
{
  msgs::Response response;
  response.set_id(_msg->id());
  response.set_request(_msg->request());
  response.set_response("success");
  std::string *serializedData = response.mutable_serialized_data();

  if (_msg->request() == "physics_info")
  {
    OPENSIM_PHYSICS_LOG_CALL();
    msgs::Physics physicsMsg;
    physicsMsg.set_type(msgs::Physics::SIMBODY);
    // min_step_size is defined but not yet used
    physicsMsg.set_min_step_size(this->GetMaxStepSize());
    physicsMsg.set_enable_physics(this->world->PhysicsEnabled());

    physicsMsg.mutable_gravity()->CopyFrom(
      msgs::Convert(this->world->Gravity()));
    physicsMsg.mutable_magnetic_field()->CopyFrom(
      msgs::Convert(this->world->MagneticField()));
    physicsMsg.set_real_time_update_rate(this->realTimeUpdateRate);
    physicsMsg.set_real_time_factor(this->targetRealTimeFactor);
    physicsMsg.set_max_step_size(this->maxStepSize);

    response.set_type(physicsMsg.GetTypeName());
    physicsMsg.SerializeToString(serializedData);
    this->responsePub->Publish(response);
  }
}

/////////////////////////////////////////////////
void OpensimPhysics::OnPhysicsMsg(ConstPhysicsPtr &_msg)
{
  OPENSIM_PHYSICS_LOG_CALL();

  // Parent class handles many generic parameters
  // This should be done first so that the profile settings
  // can be over-ridden by other message parameters.
  PhysicsEngine::OnPhysicsMsg(_msg);

  if (_msg->has_enable_physics())
    this->world->SetPhysicsEnabled(_msg->enable_physics());

  if (_msg->has_gravity())
    this->SetGravity(msgs::ConvertIgn(_msg->gravity()));

  if (_msg->has_real_time_factor())
    this->SetTargetRealTimeFactor(_msg->real_time_factor());

  if (_msg->has_real_time_update_rate())
    this->SetRealTimeUpdateRate(_msg->real_time_update_rate());

  if (_msg->has_max_step_size())
    this->SetMaxStepSize(_msg->max_step_size());

  /* below will set accuracy for opensim if the messages exist
  // Set integrator accuracy (measured with Richardson Extrapolation)
  if (_msg->has_accuracy())
  {
    this->integ->setAccuracy(_msg->opensim().accuracy());
  }

  // Set stiction max slip velocity to make it less stiff.
  if (_msg->has_max_transient_velocity())
  {
    this->contact.setTransitionVelocity(
    _msg->opensim().max_transient_velocity());
  }
  */

  /// Make sure all models get at least on update cycle.
  this->world->EnableAllModels();
}

//////////////////////////////////////////////////
void OpensimPhysics::Reset()
{
  OPENSIM_PHYSICS_LOG_CALL();

  boost::recursive_mutex::scoped_lock lock(
    *this->GetPhysicsUpdateMutex());

  if (!this->opensimPhysicsInitialized)
  {
    gzerr << "Ignored attempt to reset the world while not properly initialized." << std::endl;
    return;
  }

  // Restore potentially user run-time modified gravity
  // This fetches the gravity vector from the SDF tree.
  this->SetGravity(this->world->Gravity());
  this->opensimPhysicsInitialized = false;
  // Just rebuild everything.
  this->SystemUpdateAfterTopologyChange();
  this->opensimPhysicsInitialized = true;
}

//////////////////////////////////////////////////
void OpensimPhysics::Init()
{
  OPENSIM_PHYSICS_LOG_CALL();

  boost::recursive_mutex::scoped_lock lock(
    *this->GetPhysicsUpdateMutex());

  this->dataPtr->gravity_vector = Vector3ToVec3(this->world->Gravity());
  this->SystemUpdateAfterTopologyChange();
  this->opensimPhysicsFirstStart = false;
  this->opensimPhysicsInitialized = true;
}

//////////////////////////////////////////////////
void OpensimPhysics::SystemUpdateAfterTopologyChange()
{
  OPENSIM_PHYSICS_LOG_CALL();
#if 1
  OPENSIM_PHYSICS_DEBUG1(
  // Additional debug output of items in the OpenSim model instance.
  {
    const auto &osimModel = this->dataPtr->osimModel;
    gzdbg << "--- Opensim::Model dump: " << osimModel.getName() << " ---" << std::endl;
    const auto &bodySet   = osimModel.getBodySet();
    for (int i=0; i<bodySet.getSize(); ++i)
    {
      const auto &body = bodySet[i];
      gzdbg << "  B:" << body.getName() << std::endl;
      const auto &wrapSet = body.getWrapObjectSet();
      for (int j=0; j<wrapSet.getSize(); ++j)
      {
        auto &wrapObj = wrapSet[j];
        gzdbg << "    W:" << wrapObj.getName() << std::endl;
      }
    }
    const auto forceSet = osimModel.getForceSet();
    for (int i=0; i<forceSet.getSize(); ++i)
    {
      auto &force = forceSet[i];
      gzdbg << "  F:" << force.getName() << std::endl;
    }
    const auto constraintSet = osimModel.getConstraintSet();
    for (int i=0; i<constraintSet.getSize(); ++i)
    {
      auto &constraint = constraintSet[i];
      gzdbg << "  C:" << constraint.getName() << std::endl;
    }
    gzdbg << "---------------------------------------" << std::endl;
  })
#endif
  /* Deletes and recreates internal simbody system instances -
   * MultibodySystem, MatterSubsystem, and so on. Invalidates
   * existing references */
  try
  {
    this->dataPtr->osimModel.buildSystem();
    /* Regenerate dependees */
    this->dataPtr->RegenerateIntegrator();
    // buildSystem deletes the internal gravity force instance.
    // It does not memorize the gravity for the new system.
    this->dataPtr->osimModel.updGravityForce().
      setDefaultGravityVector(
          this->dataPtr->gravity_vector);
    // Realizes the system up to position stage and does tons of other stuff.
    SimTK::GeneralForceSubsystem &fs = this->dataPtr->osimModel.updForceSubsystem();
    const SimTK::SimbodyMatterSubsystem &ms = this->dataPtr->osimModel.getMatterSubsystem();
    this->dataPtr->discreteForces = SimTK::Force::DiscreteForces(fs, ms);
    this->dataPtr->osimModel.initializeState();
    // Want to realize system this far for when the state is queried before UpdatePhysics is called.
    this->dataPtr->osimModel.getMultibodySystem().realize(
    this->dataPtr->osimModel.getWorkingState(),
      SimTK::Stage::Acceleration);
  }
  catch(const std::exception &e)
  {
    gzerr << "Fatal exception in OpensimPhysics initialization: " << typeid(e).name() << ":" << e.what() << std::endl << ". Exiting now!" << std::endl;
    exit(-1);
    // TODO: Make non-fatal: Call RemoveModel right after AddModel failed. Or something like that.
  }
  catch(...)
  {
    gzerr << "Fatal exception of unidentified type in OpensimPhysics initialization. Exiting now!" << std::endl;
    exit(-1);
  }
}


namespace gazebo { namespace physics {

void SaveLinkCoordinatesInDefaultValues(Model &_model, SimTK::State &state)
{
  physics::Link_V links = _model.GetLinks();
  for (physics::Link_V::iterator lx = links.begin(); lx != links.end(); ++lx)
  {
    OpensimLinkPtr opensimLink =
      boost::dynamic_pointer_cast<physics::OpensimLink>(*lx);

    opensimLink->GetPriv()->SaveCoordinatesInDefaultValues(state);
  }
}

// TODO: I have a feeling that a lot of this stuff could move into a new pimpl class of OpensimModel ...
void SaveInitialCoordinatesAndAddSlaveMasterWelds(Model &_model, OpenSim::Model &osimModel)
{
  const auto &links = _model.GetLinks();
  for (auto lx = links.begin(); lx != links.end(); ++lx)
  {
    auto link_priv = static_cast<OpensimLink&>(**lx).GetPriv();
    // Saving is only relevant for Free mobilizers, since all other
    // initial coordinates are zero by design. Regardless we call this function always.
    link_priv->SaveInitialCoordinates();
    link_priv->AddSlaveMasterWeldConstraintsTo(osimModel);
  }
}


}} // namespaces


//////////////////////////////////////////////////
void OpensimPhysics::AddModelHierarchyToOpensimSystem(OpensimModel *_model)
{
  OPENSIM_PHYSICS_LOG_CALL_ON(_model->GetName());
  boost::recursive_mutex::scoped_lock lock(
  *this->GetPhysicsUpdateMutex());
  this->opensimPhysicsInitialized = false;

  // First save the state of all mobilizers.
  // However if we start from scratch we first add all the models and then initialize the system.
  // Hence the SimTK state is only valid if we had a running system before.
  if (!this->opensimPhysicsFirstStart)
  {
    auto &state = this->dataPtr->osimModel.updWorkingState();
    if (state.getSystemStage() != SimTK::Stage::Empty)
    {
      for (ModelPtr other_model : this->world->Models())
      {
        // The SimTK state does not yet have the degrees of freedom of the new model.
        // So we skip the new model.
        if (other_model.get() != _model)
        {
          ForTheModelHierarchy(*other_model,
            [&state](Model &_local_model)
            {
              SaveLinkCoordinatesInDefaultValues(_local_model, state);
            });
        }
      }
    }
  }

  try
  {
    SimTK::MultibodyGraphMaker mbgraph = MultibodyGraphBuilding::CreateMultibodyGraph(*_model);
    // Optional: dump the graph to stdout for debugging or curiosity.
    // gzdbg << "----------- mbgraph dump ------------\n";
    // mbgraph.dumpGraph(gzdbg);
    // gzdbg << "----------- mbgraph dump end --------\n";
    this->AddDynamicModelsToOpensimSystem(mbgraph);

    ForTheModelHierarchy(static_cast<Model&>(*_model),
      [&mbgraph,this](Model &_local_model)
      {
        auto &opensimmodel = static_cast<OpensimModel&>(_local_model);
        if (opensimmodel.IsStatic())
        {
          this->AddStaticModelToOpensimSystem(opensimmodel);
        }
        else
        {
          this->GetPriv()->LoadMuscleDefinitionsAndDoSanityCheck(opensimmodel);
          SaveInitialCoordinatesAndAddSlaveMasterWelds(_local_model, this->GetPriv()->osimModel);
        }
      }
    );
  }
  catch(const std::exception& e)
  {
    gzthrow(std::string("Opensim build EXCEPTION: ") + e.what());
  }

  /* NOTE: The equivalent Simbody call would be this->system.realizeTopology();.
   * However, Simbody wants us to recreate the sytem from scratch which requires
   * a more elaborate setup routine. TODO: Really? Maybe the OpenSim provides
   * a nicer (faster) way after all.
   * We do this only if Init has been called already. Otherwise we are in the
   * process of loading a world. So Init will be called later which then invokes
   * SystemUpdateAfterTopologyChange().
   */
  if (!this->opensimPhysicsFirstStart)
  {
    this->SystemUpdateAfterTopologyChange();
    this->opensimPhysicsInitialized = true;
  }
}


//////////////////////////////////////////////////
void OpensimPhysics::InitForThread()
{
  /* This is called relatively late after the world has constructed. */
}

//////////////////////////////////////////////////
void OpensimPhysics::UpdateCollision()
{
  boost::recursive_mutex::scoped_lock lock(*this->physicsUpdateMutex);
  this->contactManager->ResetCount();

  try
  {
    using namespace SimTK::GeneralContactForceFeedback;
    OPENSIM_CONTACT_DEBUG_UPDATE_LOOP(gzdbg << "CONTACTS" << std::endl;)
    Contact *contactFeedback = nullptr;
    this->dataPtr->contactForceManager.UpdateContactData();
    auto begin = this->dataPtr->contactForceManager.BeginContactData();
    auto end   = this->dataPtr->contactForceManager.EndContactData();
    for (auto it = begin; it != end; ++it)
    {
      const ContactDatum& cd = *it;
      OpensimCollision* collision1 = static_cast<OpensimCollision*>(cd.userDataGeom1);
      OpensimCollision* collision2 = static_cast<OpensimCollision*>(cd.userDataGeom2);
      //Contact *contactFeedback = this->contactManager->NewContact(collision1, collision2, this->world->GetSimTime());
      if (!contactFeedback ||
          collision1 != contactFeedback->collision1 ||
          collision2 != contactFeedback->collision2)
      {
        contactFeedback = this->contactManager->NewContact(collision1, collision2, this->world->SimTime());
      }
      if (contactFeedback && contactFeedback->count < MAX_CONTACT_JOINTS)
      {
        const SimTK::Real dist = cd.distance;
        const SimTK::Vec3& np_body2 = cd.nearestPointBody2;
        const SimTK::Vec3& normalVec = cd.normal;
        const SimTK::Vec3& forceVec = cd.force;
        OPENSIM_CONTACT_DEBUG_UPDATE_LOOP(
          gzdbg << "Contact" << (it - begin)  << ": " << collision1->GetName() << "<->" << collision2->Name() << "  f=" << forceVec << std::endl;)

        contactFeedback->normals[contactFeedback->count] = ignition::math::Vector3d(normalVec[0], normalVec[1], normalVec[2]);
        contactFeedback->depths[contactFeedback->count] = dist;
        contactFeedback->positions[contactFeedback->count] = ignition::math::Vector3d(np_body2[0], np_body2[1], np_body2[2]);
        contactFeedback->wrench[contactFeedback->count].body1Force = ignition::math::Vector3d(forceVec[0], forceVec[1], forceVec[2]);
        contactFeedback->wrench[contactFeedback->count].body2Force = ignition::math::Vector3d(-forceVec[0], -forceVec[1], -forceVec[2]);
        /// TODO: Torque; not directly computed by OpenSim
        contactFeedback->count++;
      }
    }
  }
  catch (SimTK::Exception::CacheEntryOutOfDate& ex)
  {
    gzerr << "Fallthrough exception querying MobilizedBody properties: " << ex.what() << std::endl;
  }
  catch (SimTK::Exception::StageTooLow& ex)
  {
    gzerr << "Fallthrough exception querying MobilizedBody properties: " << ex.what() << std::endl;
  }
  catch (OpenSim::Exception& ex)
  {
    gzerr << "OpenSim exception occurred in " << __FUNCTION__ << ": " << ex.getMessage() << std::endl;
  }
}

//////////////////////////////////////////////////

OPENSIM_PHYSICS_DEBUG_UPDATE_LOOP(
namespace {
void dumpIntegratorStats(SimTK::Integrator& integ) {
    printf("Used Integrator %s at accuracy %g:\n",
        integ.getMethodName(), integ.getAccuracyInUse());
    printf("# STEPS/ATTEMPTS = %d/%d\n",  integ.getNumStepsTaken(),
                                          integ.getNumStepsAttempted());
    printf("# ERR TEST FAILS = %d\n",     integ.getNumErrorTestFailures());
    printf("# REALIZE/PROJECT = %d/%d\n", integ.getNumRealizations(),
                                          integ.getNumProjections());
    printf("  PREVIOUS STEP SIZE = %f\n", integ.getPreviousStepSizeTaken());
    integ.resetAllStatistics();
}
}
)

namespace gazebo { namespace physics {
class OpensimUpdatingPhysics
{
  OPENSIM_PHYSICS_DEBUG_UPDATE_LOOP(std::stringstream os;)
  WorldPtr world;
  const SimTK::State &state;
public:
  OpensimUpdatingPhysics(WorldPtr _world, const SimTK::State &_state)
    : world(_world), state(_state)
  {
    OPENSIM_PHYSICS_DEBUG_UPDATE_LOOP(DebugPrintState();)
  }

  ~OpensimUpdatingPhysics()
  {
    OPENSIM_PHYSICS_DEBUG_UPDATE_LOOP(gzdbg << os.str() << std::endl;);
  }

  OPENSIM_PHYSICS_DEBUG_UPDATE_LOOP(
  void DebugPrintState()
  {
    os << "t = " << state.getTime() << "\n";
    os << "\tQ    = " << state.getQ() << std::endl;
    os << "\tU    = " << state.getU() << std::endl;
    os << "\tUdot = " << state.getUDot() << std::endl;
  })

  OPENSIM_PHYSICS_DEBUG_UPDATE_LOOP(
  void DebugPrintLink(const OpensimLink &link)
  {
    // Output joint coordinates if this body is not static.
    os << "\tLink: " << link.GetScopedName() << std::endl;
    for (unsigned int k = 0; k < link.GetPriv()->mobs.size(); ++k)
    {
      const Mob &mob = link.GetPriv()->mobs[k];
      const bool is_master = link.GetPriv()->master_index == (int)k;
      os << "\tmobilizer " << k << ": " << FormatName(mob.gzJoint) << (is_master ? " (master)" : " (slave)") << std::endl;
      auto &sim_tk_mob = link.GetPriv()->getSimTKBody();
      auto Xgb = sim_tk_mob.getBodyTransform(state);
      auto Xbm = sim_tk_mob.getOutboardFrame(state);
      os << "\txgb = \n" << prettyPrintTransform(Xgb) << std::endl;
      os << "\txgm = \n" << prettyPrintTransform(Xgb * Xbm) << std::endl;
      auto &osimJoint = *mob.joint.get();
      const OpenSim::CoordinateSet& jc = osimJoint.get_CoordinateSet();
      for (int i=0; i<osimJoint.numCoordinates(); ++i)
      {
        const OpenSim::Coordinate &c = jc[i];
        os << "\tC[" << c.getName() << "] = " << c.getValue(state) << std::endl;
      }
    }
  })


  void PushPosesOfModelForVisualization(const Model &_model)
  {
    physics::Link_V links = _model.GetLinks();
    for (physics::Link_V::iterator lx = links.begin(); lx != links.end(); ++lx)
    {
      physics::OpensimLinkPtr link =
      boost::dynamic_pointer_cast<physics::OpensimLink>(*lx);

      //auto &osimBody = link->GetPriv()->getOsimBody();
      auto bodyTrafo = link->GetPriv()->getSimTkTransform(state);
      ignition::math::Pose3d pose = Transform2Pose(bodyTrafo);

      OPENSIM_PHYSICS_DEBUG_UPDATE_LOOP(
        DebugPrintLink(*link);
      );

      link->SetDirtyPose(pose);
      this->world->_AddDirty(
        boost::static_pointer_cast<Entity>(*lx).get());
    }
  }

  void PushPosesOfModelsInTheWorld()
  {
    physics::Model_V models = this->world->Models();
    for (auto model : models)
    {
      ForTheConstModelHierarchy(*model,
        [this](const Model &_model)
        {
          PushPosesOfModelForVisualization(_model);
        }
      );
    }
  }
};


class MuscleMessagePublisher
{
  gazebo::transport::PublisherPtr muscleInfoPublisher;
  common::Time simTime;
  msgs::OpenSimMuscles muscles_msg;

public:
  MuscleMessagePublisher(
    gazebo::transport::PublisherPtr _muscleInfoPublisher,
    common::Time _simTime)
    : muscleInfoPublisher(_muscleInfoPublisher),
      simTime(_simTime)
      {}

  void PublishOnModel(Model &_model_base)
  {
    auto &model = static_cast<OpensimModel&>(_model_base);

    // Don't process further if the model has no muscles
    if (model.GetMuscles().empty())
      return;

    //msgs::OpenSimMuscles muscles_msg;
    std::vector<ignition::math::Vector3d> muscle_path_buf;

    for (auto muscle : model.GetMuscles())
    {
      msgs::OpenSimMuscle* muscle_msg = muscles_msg.add_muscle();
      muscle->GetCurrentWorldPath(muscle_path_buf);

      GZ_ASSERT(muscle_path_buf.size() >= 2, "Muscles are supposed to have a start node and an end node");
      for (std::size_t i=0; i<muscle_path_buf.size(); ++i)
      {
        msgs::Vector3d* point = muscle_msg->add_pathpoint();
        msgs::Set(
          point,
          muscle_path_buf[i]);
      }

      // For a bit better performance we could access the internal OpenSim::Muscle pointer directly.
      muscle_msg->set_length(muscle->GetLength());
      muscle_msg->set_activation(muscle->GetActivationValue());
      muscle_msg->set_ismuscle(muscle->IsMuscle());
      OPENSIM_MUSCLE_DEBUG_UPDATE_LOOP(
        gzdbg << model.GetScopedName() << "/" << muscle->GetName() << ", length=" << muscle_msg->length() << std::endl;)
    }
  }


  void PublishOnAllModels(World &world)
  {
    muscles_msg = msgs::OpenSimMuscles();
    muscles_msg.set_robot_name("all_of_them");
    for (auto model_base : world.Models())
    {
      ForTheModelHierarchy(*model_base,
        [this](Model &_local_model)
        {
          this->PublishOnModel(_local_model);
        });
    }
    msgs::Set(muscles_msg.mutable_time(), this->simTime);
    this->muscleInfoPublisher->Publish(muscles_msg);
  }
};

}} // namespaces

void OpensimPhysics::UpdatePhysics()
{
#ifdef TIME_UPDATE_PHYSICS
  gazebo::common::Time _tstart = gazebo::common::Time::GetWallTime();
#endif

  boost::recursive_mutex::scoped_lock lock(*this->physicsUpdateMutex);

  OPENSIM_PHYSICS_DEBUG_UPDATE_LOOP(
    gzdbg << __FUNCTION__ << " in thread " << std::this_thread::get_id() << std::endl;
    gzdbg << "UpdatePhysics: t=" << this->world->SimTime().Double() << std::endl;)
  
  if (!this->opensimPhysicsInitialized)
  {
    gzdbg << __FUNCTION__ << " before OpensimPhysics was initialized" << std::endl;
    return;
  }

  auto &state = this->dataPtr->osimModel.updWorkingState();

  this->dataPtr->osimManager.setInitialTime(this->dataPtr->osimManager.getFinalTime());
  this->dataPtr->osimManager.setFinalTime(this->world->SimTime().Double());
  // This manager class is full of crap ... don't look inside there be dragons!
  // TODO: Implement maximal time step, which is stored in variable this->stepTimeDouble
  const bool ok = this->dataPtr->osimManager.integrate(state);
  if (!ok)
    gzerr << "Opensim Manager::integrate failed.\n";

  OPENSIM_PHYSICS_DEBUG_UPDATE_LOOP(dumpIntegratorStats(*this->dataPtr->integrator););

  this->dataPtr->osimModel.getMultibodySystem().realize(state, SimTK::Stage::Acceleration);

  {
    OpensimUpdatingPhysics updating_physics(this->world, state);
    updating_physics.PushPosesOfModelsInTheWorld();
  }

  OPENSIM_PHYSICS_DEBUG_UPDATE_LOOP(
    const SimTK::SimbodyMatterSubsystem &matter = this->dataPtr->osimModel.getMatterSubsystem();
    for (int i = 0; i < matter.getNumConstraints(); ++i)
    {
      if (!SimTK::Constraint::Weld::isInstanceOf(matter.getConstraint(SimTK::ConstraintIndex(i))))
        continue;
      const auto &c = SimTK::Constraint::Weld::downcast(matter.getConstraint(SimTK::ConstraintIndex(i)));
      gzdbg << "Weld Constraint " << i << std::endl;
      gzdbg << "\tPos Errors " << c.getPositionErrors(state) << std::endl;
      gzdbg << "\tVel Errors " << c.getVelocityErrors(state) << std::endl;
      //gzdbg << "\tReaction1=" << c.getWeldReactionOnBody1(state) << std::endl;
      //gzdbg << "\tReaction2=" << c.getWeldReactionOnBody2(state) << std::endl;
    }
  )

  if (this->muscleInfoPublisher && !transport::getMinimalComms())
  {
    MuscleMessagePublisher publisher{this->muscleInfoPublisher, this->world->SimTime()};
    publisher.PublishOnAllModels(*this->world);
  }

#ifdef OPENSIM_USE_DEBUG_DRAWER
  // Send debug drawer data when debug drawer is active
  if (this->GetDebugDrawer() != NULL)
  {
    const SimTK::GeneralContactSubsystem &contactSubsystem = this->dataPtr->osimModel.getMultibodySystem().getContactSubsystem();
    for (int k = 0; k < contactSubsystem.getNumContactSets(); ++k)
    {
      SimTK::ContactSetIndex contactSetInd(k);
      if (contactSetInd.isValid())
      {
        gzdbg << "   Contact set "<< k << " contains " << contactSubsystem.getNumBodies(contactSetInd) << " bodies, with transforms:   " << std::endl;

        for (int m = 0; m < contactSubsystem.getNumBodies(contactSetInd); ++m)
        {
          SimTK::ContactSurfaceIndex contactSurfaceInd(m);
          if (contactSurfaceInd.isValid())
          {
            const SimTK::MobilizedBody mb = contactSubsystem.getBody(contactSetInd, contactSurfaceInd);
            gzdbg << m << ": "<< mb.getBodyTransform(state) << std::endl;

            SimTK::ContactSurfaceIndex bodyIndex = contactSurfaceInd;
            SimTK::GeneralContactSubsystem subsystem = contactSubsystem;
            SimTK::ContactSetIndex set = contactSetInd;

            if (subsystem.getBodyGeometry(set, bodyIndex).getTypeId() == SimTK::ContactGeometry::TriangleMesh::classTypeId())
            {
              SimTK_APIARGCHECK1(bodyIndex >= 0 && bodyIndex < subsystem.getNumBodies(set), "\"not actually a Simbody API method, but OpenSimSimulation\"", "Init",
                                 "Illegal body index: %d", (int)bodyIndex);
              SimTK_APIARGCHECK1(subsystem.getBodyGeometry(set, bodyIndex).getTypeId()
                                 == SimTK::ContactGeometry::TriangleMesh::classTypeId(),
                                 "\"not actually a Simbody API method, but OpenSimSimulation\"", "Init",
                                 "Body %d is not a triangle mesh", (int)bodyIndex);

              const SimTK::ContactGeometry::TriangleMesh& triangle_mesh =
                  SimTK::ContactGeometry::TriangleMesh::getAs(subsystem.getBodyGeometry(set, bodyIndex));

              for (int i = 0; i < (int) triangle_mesh.getNumFaces(); i++)
              {
                const SimTK::Vec3& v0 = triangle_mesh.getVertexPosition(triangle_mesh.getFaceVertex(i, 0));
                const SimTK::Vec3& v1 = triangle_mesh.getVertexPosition(triangle_mesh.getFaceVertex(i, 1));
                const SimTK::Vec3& v2 = triangle_mesh.getVertexPosition(triangle_mesh.getFaceVertex(i, 2));

               this->dataPtr->debugDrawer->drawTriangle(ignition::math::Vector3d(v0[0], v0[1], v0[2]),
                    ignition::math::Vector3d(v1[0], v1[1], v1[2]),
                    ignition::math::Vector3d(v2[0], v2[1], v2[2]),
                    ignition::math::Vector3d(1,0.2,0.2),
                    1.0);
              }
            }

          }
        }
      }
    }

    msgs::ContactDebug msg;
    {
      const std::vector<Ogre::Vector3>& debugPoints = this->GetDebugDrawer()->getPoints();
      const std::vector<Ogre::Vector3>& debugPointColors = this->GetDebugDrawer()->getPointColors();

      const std::vector<Ogre::Vector3>& debugLines = this->GetDebugDrawer()->getLines();
      const std::vector<Ogre::Vector3>& debugLineColors = this->GetDebugDrawer()->getLineColors();

      const std::vector<Ogre::Vector3>& debugTriangles = this->GetDebugDrawer()->getTriangles();
      const std::vector<Ogre::Vector3>& debugTriangleColors = this->GetDebugDrawer()->getTriangleColors();

      msg.set_pointcount(debugPoints.size());
      msg.set_linecount(debugLines.size() / 2);
      msg.set_trianglecount(debugTriangles.size() /*/ 3*/);
      if (debugPoints.size() > 0)
      {
        for (unsigned int i = 0; i < debugLines.size(); ++i)
        {
          msgs::Vector3d *debugDrawVec = msg.add_points();
          debugDrawVec->set_x(debugPoints[i].x);
          debugDrawVec->set_y(debugPoints[i].y);
          debugDrawVec->set_z(debugPoints[i].z);

          msgs::Vector3d *colorVec = msg.add_point_colors();
          colorVec->set_x(debugPointColors[i].x);
          colorVec->set_y(debugPointColors[i].y);
          colorVec->set_z(debugPointColors[i].z);
        }
      }

      if (debugLines.size() > 0)
      {
        for (unsigned int i = 0; i < debugLines.size(); ++i)
        {
          msgs::Vector3d *debugDrawVec = msg.add_line_points();
          debugDrawVec->set_x(debugLines[i].x);
          debugDrawVec->set_y(debugLines[i].y);
          debugDrawVec->set_z(debugLines[i].z);
        }

        for (unsigned int i = 0; i < debugLineColors.size(); ++i)
        {
          msgs::Vector3d *colorVec = msg.add_line_colors();
          colorVec->set_x(debugLineColors[i].x);
          colorVec->set_y(debugLineColors[i].y);
          colorVec->set_z(debugLineColors[i].z);
        }
      }

      if (debugTriangles.size() > 0)
      {
        for (unsigned int i = 0; i < debugTriangles.size(); ++i)
        {
          msgs::Vector3d *debugDrawVec = msg.add_triangle_points();
          debugDrawVec->set_x(debugTriangles[i].x);
          debugDrawVec->set_y(debugTriangles[i].y);
          debugDrawVec->set_z(debugTriangles[i].z);
        }

        for (unsigned int i = 0; i < debugTriangleColors.size(); ++i)
        {
          msgs::Vector3d *colorVec = msg.add_triangle_colors();
          colorVec->set_x(debugTriangleColors[i].x);
          colorVec->set_y(debugTriangleColors[i].y);
          colorVec->set_z(debugTriangleColors[i].z);
        }
      }

      this->getDebugDrawPub()->Publish(msg);

      this->GetDebugDrawer()->clear(true);
    }
  }
#endif //OPENSIM_USE_DEBUG_DRAWER

      //TODO: implement force "feedback"
      //     physics::Joint_V joints = (*mi)->GetJoints();
      //     for (physics::Joint_V::iterator jx = joints.begin(); jx != joints.end(); ++jx)
      //     {
      //       OpensimJointPtr opensimJoint =
      //         boost::dynamic_pointer_cast<physics::OpensimJoint>(*jx);
      //       opensimJoint->CacheForceTorque();
      //     }
  //   // FIXME:  this needs to happen before forces are applied for the next step
  //   // FIXME:  but after we've gotten everything from current state
  //   this->discreteForces.clearAllForces(this->integ->updAdvancedState());

#ifdef TIME_UPDATE_PHYSICS
  gazebo::common::Time _tend = gazebo::common::Time::GetWallTime();
  this->GetPriv()->addUpdateCall(_tend-_tstart);
  gzdbg << "Update time: " << this->GetPriv()->getMeanUpdateTime() << " +/- "
        << this->GetPriv()->getStdOfMeanUpdateTime() << std::endl;
#endif
}

//////////////////////////////////////////////////
void OpensimPhysics::Fini()
{
  OPENSIM_PHYSICS_LOG_CALL();
  PhysicsEngine::Fini();

#ifdef OPENSIM_USE_DEBUG_DRAWER
  delete this->dataPtr->debugDrawer;
  this->dataPtr->debugDrawer = NULL;
#endif
}

//////////////////////////////////////////////////
ModelPtr OpensimPhysics::CreateModel(BasePtr _parent)
{
  /* Apparently the parent can be different things. A world, another model,
   * or a thing called Population. Possibly other things. See sdformat.org. */
  OPENSIM_PHYSICS_DEBUG1(gzdbg << __FUNCTION__ << " parent = " << (_parent ? _parent->Name() : "nullptr") << std::endl;)

  OpensimModelPtr model(new OpensimModel(_parent, this));

  return model;
}

//////////////////////////////////////////////////
LinkPtr OpensimPhysics::CreateLink(ModelPtr _parent)
{
  OPENSIM_PHYSICS_DEBUG1(gzdbg << __FUNCTION__ << " parent = " << (_parent ? _parent->Name() : "nullptr") << std::endl;)
  if (_parent == NULL)
    gzthrow("Link must have a parent\n");

  OpensimLinkPtr link(new OpensimLink(_parent, this));
  link->SetWorld(_parent->GetWorld());

  return link;
}

//////////////////////////////////////////////////
CollisionPtr OpensimPhysics::CreateCollision(const std::string &_type,
                                            LinkPtr _parent)
{
  OPENSIM_PHYSICS_DEBUG1(gzdbg << __FUNCTION__ << " type = " << _type << " parent = " << (_parent ? _parent->GetName() : "nullptr") << std::endl;)
  OpensimCollisionPtr collision(new OpensimCollision(_parent));
  ShapePtr shape = this->CreateShape(_type, collision);
  collision->SetShape(shape);
  shape->SetWorld(_parent->GetWorld());
  return collision;
}

//////////////////////////////////////////////////
ShapePtr OpensimPhysics::CreateShape(const std::string &_type,
                                    CollisionPtr _collision)
{
  OPENSIM_PHYSICS_DEBUG1(gzdbg << __FUNCTION__ << " type = " << _type << std::endl;)
  ShapePtr shape;
  OpensimCollisionPtr collision =
    boost::dynamic_pointer_cast<OpensimCollision>(_collision);

  if (_type == "plane")
  {
    shape.reset(new OpensimPlaneShape(collision));
  }
  else if (_type == "sphere")
  {
    shape.reset(new OpensimSphereShape(collision));
  }
  else if (_type == "box")
  {
    shape.reset(new OpensimBoxShape(collision));
  }
  else if (_type == "cylinder")
  {
    shape.reset(new OpensimCylinderShape(collision));
  }
  else if (_type == "mesh" || _type == "trimesh")
  {
    shape.reset(new OpensimMeshShape(collision));
  }
  else if (_type == "polyline")
  {
    shape.reset(new OpensimPolylineShape(collision));
  }
  else if (_type == "heightmap")
  {
    shape.reset(new OpensimHeightmapShape(collision));
  }
  else if (_type == "multiray")
  {
    shape.reset(new OpensimMultiRayShape(collision));
  }
  else if (_type == "ray")
  {
    if (_collision)
      shape.reset(new OpensimRayShape(_collision));
    else
      shape.reset(new OpensimRayShape(this->world->Physics()));
  }
  else
  {
    //shapeTypeValid = false;
    gzerr << "Unable to create collision of type[" << _type << "]\n";
  }
  return shape;
}

//////////////////////////////////////////////////
JointPtr OpensimPhysics::CreateJoint(const std::string &_type,
                                     ModelPtr _parent)
{
  OPENSIM_PHYSICS_DEBUG1(gzdbg << __FUNCTION__ << " type = " << _type << std::endl;)
  JointPtr joint;

  if (_type == "revolute")
    joint = boost::make_shared<OpensimHingeJoint>(_parent, *this);
  else if (_type == "ball")
    joint = boost::make_shared<OpensimBallJoint>(_parent, *this);
//   else if (_type == "universal")
//     joint.reset(new OpensimUniversalJoint(this->dynamicsWorld, _parent));
  else if (_type == "prismatic")
    joint = boost::make_shared<OpensimSliderJoint>(_parent, *this);
//   else if (_type == "revolute2")
//     joint.reset(new OpensimHinge2Joint(this->dynamicsWorld, _parent));
//   else if (_type == "screw")
//     joint.reset(new OpensimScrewJoint(this->dynamicsWorld, _parent));
  else if (_type == "fixed")
    joint.reset(new OpensimFixedJoint(_parent, *this));
  else
    gzthrow("Joint type [" << _type << "] not yet implemented!");

  return joint;
}

//////////////////////////////////////////////////
void OpensimPhysics::SetGravity(const ignition::math::Vector3d &_gravity)
{
  /* Can be called from a thread different from where the main physics loop runs in?! */
  OPENSIM_PHYSICS_DEBUG1(gzdbg << __FUNCTION__ << " g = " << _gravity << std::endl;)

  boost::recursive_mutex::scoped_lock lock(*this->physicsUpdateMutex);

  /* The old code used the SDF to store the gravity vector. But this was
   * not feasible since SetGravity is called by SystemUpdateAfterTopologyChange
   * after the last model was removed when destroying a world. At this time
   * there is no valid SDF and world->Get/SetGravity will crash!!
   */
  this->dataPtr->gravity_vector = Vector3ToVec3(_gravity);

  if (!this->opensimPhysicsFirstStart && this->dataPtr->osimModel.isValidSystem())
  {
    auto &gravity = this->dataPtr->osimModel.updGravityForce(); // Should be available after since we called osimModel.buildsystem()
    gravity.setGravityVector(
      this->dataPtr->osimModel.updWorkingState(),
      Vector3ToVec3(_gravity));
    // Might need this according to https://simtk.org/api_docs/simbody/3.5/classSimTK_1_1Integrator.html
    //this->integrator->reinitialize(SimTK::Stage::Dynamics, false);
  }
}

//////////////////////////////////////////////////
void OpensimPhysics::DebugPrint() const
{
}

//////////////////////////////////////////////////
void OpensimPhysics::AddStaticModelToOpensimSystem(OpensimModel &_model)
{
  /* Just adds collision geometry to the ground body (??) */
  OPENSIM_PHYSICS_DEBUG1(gzdbg << __FUNCTION__ << " on " << _model.GetName() << std::endl;)

  physics::Link_V links = _model.GetLinks();
  for (physics::Link_V::iterator li = links.begin(); li != links.end(); ++li)
  {
    OpensimLinkPtr opensimLink = boost::dynamic_pointer_cast<OpensimLink>(*li);
    if (opensimLink)
    {
      GZ_ASSERT(opensimLink->GetPriv()->master_index == MOB_INDEX_UNINITIALIZED, "Should be uninitialized");
      opensimLink->GetPriv()->master_index = MOB_INDEX_STATIC_GROUND;

      this->AddLinkCollisions(*opensimLink, this->dataPtr->osimModel.getGroundBody(), SimTK::ContactCliqueId());
    }
    else
      gzerr << "opensimLink [" << (*li)->GetName() << "]is not a OpensimLinkPtr\n";
  }

  Joint_V joints = _model.GetJoints();
  for (auto it = joints.begin(); it != joints.end(); ++it)
  {
    auto joint = boost::static_pointer_cast<OpensimJoint>(*it);
    joint->GetPriv()->mob_index = MOB_INDEX_STATIC_GROUND;
  }
}


//////////////////////////////////////////////////
template<class JointType>
static boost::shared_ptr<OpenSim::Joint> JointFactory(const SimTK::Transform &Xbm,
                                               const SimTK::Transform &Xpf,
                                               OpenSim::Body &parent,
                                               OpenSim::Body &body)
{
  Vec3 Xbm_t, Xpf_t, Xbm_angles, Xpf_angles;
  Xbm_t = Xbm.p();
  Xbm_angles = Xbm.R().convertRotationToBodyFixedXYZ();
  Xpf_t = Xpf.p();
  Xpf_angles = Xpf.R().convertRotationToBodyFixedXYZ();
    //Pose2PosAngles(Xbm_t, Xbm_angles, isReversed ? parentAnchorPose : childAnchorPose);
    //Pose2PosAngles(Xpf_t, Xpf_angles, isReversed ? childAnchorPose : parentAnchorPose);

  OPENSIM_PHYSICS_DEBUG_ADD_MODEL(gzdbg << "    Xbm = [" << Xbm_t << "," << Xbm_angles << "]" << std::endl;)
  OPENSIM_PHYSICS_DEBUG_ADD_MODEL(gzdbg << "    Xbm = \n" << prettyPrintTransform(Xbm) << std::endl;)
  OPENSIM_PHYSICS_DEBUG_ADD_MODEL(gzdbg << "    Xpf = [" << Xpf_t << "," << Xpf_angles << "]" << std::endl;)
  OPENSIM_PHYSICS_DEBUG_ADD_MODEL(gzdbg << "    Xpf = \n" << prettyPrintTransform(Xpf) << std::endl;)

  return boost::make_shared<JointType>(
    body.getName()+"->"+parent.getName(),
    parent, Xpf_t, Xpf_angles,
    body, Xbm_t, Xbm_angles);
}


//////////////////////////////////////////////////
void OpensimPhysics::AddDynamicModelsToOpensimSystem(
  const SimTK::MultibodyGraphMaker &_mbgraph)
{
  OPENSIM_PHYSICS_DEBUG_ADD_MODEL(gzdbg << __FUNCTION__ << std::endl;)
  // Generate a contact clique we can put collision geometry in to prevent
  // self-collisions.
  // \TODO: put this in a gazebo::physics::SimbodyModel class
  SimTK::ContactCliqueId modelClique = SimTK::ContactSurface::createNewContactClique();

  OPENSIM_PHYSICS_DEBUG_ADD_MODEL(gzdbg << "# mobilizers = " << _mbgraph.getNumMobilizers() << std::endl;)

  // Run through all the mobilizers in the multibody graph
  for (int mobNum = 0; mobNum < _mbgraph.getNumMobilizers(); ++mobNum)
  {
    // Get a mobilizer from the graph, then extract its corresponding
    // joint and bodies. Note that these don't necessarily have equivalents
    // in the GazeboLink and GazeboJoint inputs.
    // Also, we walk through mobilizers in topological order from near to far
    // from ground. This way, inboard bodies have already been processed.
    const SimTK::MultibodyGraphMaker::Mobilizer& mob = _mbgraph.getMobilizer(mobNum);
    const std::string& type = mob.getJointTypeName();

    // The inboard body always corresponds to one of the input links,
    // because a slave link is always the outboard body of a mobilizer.
    // The inboard body may be the ground itself, I guess. In case of
    // which gzInb == nullptr (?).
    // The outboard body may be slave, but its master body is one of the
    // Gazebo input links.
    const bool isSlave = mob.isSlaveMobilizer();
    // note: do not use boost shared pointer here, on scope out the
    // original pointer get scrambled
    OpensimLink* gzInb  = static_cast<OpensimLink*>(
      mob.getInboardBodyRef());
    OpensimLink* gzOutb = static_cast<OpensimLink*>(
      mob.getOutboardMasterBodyRef());
    // Joint might be nullptr in case this is an added base mobilizer, i.e.
    // not corresponding to a gazebo joint.
    OpensimJoint* gzJoint = static_cast<OpensimJoint*>(mob.getJointRef());

    OPENSIM_PHYSICS_DEBUG_ADD_MODEL(
      gzdbg << "mob #" << mobNum << " slave = " << mob.isSlaveMobilizer()
            << " addedBase = " << mob.isAddedBaseMobilizer() << " reversed " << mob.isReversedFromJoint() << std::endl;
      gzdbg << "  Inb : " << FormatName(gzInb) << std::endl;
      gzdbg << "  Outb: " << FormatName(gzOutb) << std::endl;

    const SimTK::MassProperties massProps =
      gzOutb->GetPriv()->GetEffectiveMassProps(mob.getNumFragments());
    )

    if (!gzOutb)
      gzthrow("Want to process a mobilizer but its outboard body link is NULL!");

    OpensimLinkPrivate *gzOutbPriv = gzOutb->GetPriv();
    gzOutbPriv->mobs.emplace_back();
    // This will reference the new mobilized body once we create it.
    Mob &osimMob = gzOutbPriv->mobs.back();
    if (!isSlave)
      gzOutbPriv->master_index = gzOutbPriv->mobs.size()-1;
    // Here it is.
    osimMob.body = boost::make_shared<OpenSim::Body>();
    osimMob.gzLink = gzOutb;
    osimMob.gzJoint = gzJoint;
    SetMassProperties(*osimMob.body, massProps);
    OPENSIM_PHYSICS_DEBUG_ADD_MODEL(
      gzdbg << "  M = " << massProps.getMass() << std::endl;
      gzdbg << "  COM = " << massProps.getMassCenter() << std::endl;
      gzdbg << "  I = " << massProps.getInertia() << std::endl;
    )
    osimMob.body->setName(this->dataPtr->CreateUniqueOsimNameFor(gzOutb));

    /* If this body connects to the ground ... */
    if (mob.isAddedBaseMobilizer())
    {
      // There is no corresponding Gazebo joint for this mobilizer.
      // Create the joint and set its default position to be the default
      // pose of the base link relative to the Ground frame.
      // Note: the inboard body is always ground!
      //GZ_ASSERT(type == "free", "Type is not 'free', not allowed.");
      //GZ_ASSERT(gzOutb != nullptr, "Want to process a link connecting to ground but the link pointer is null");
      if (type != "free")
        gzthrow("The joint type of a free body, i.e. connecting to ground should always be 'free'");
      if (gzInb)
        gzthrow("Want to process a link connecting to ground but there seems to be another body attached");

      osimMob.joint = boost::make_shared<OpenSim::FreeJoint>(
        gzOutb->GetName() + "toGround",
        this->dataPtr->osimModel.getGroundBody(), Vec3(0), Vec3(0),
        *osimMob.body, Vec3(0), Vec3(0));


      /* Invocation of addBody has to precede the use of coordinate names
       * pertaining to the mobilizer (Joint) degree of freedoms. It also
       * has to come after the creation of the joint. The coordinate names
       * won't be added to the model otherwise.
       * Therefore we put the call in every branch. */
      this->dataPtr->osimModel.addBody(osimMob.body.get());

      /* Straight from FreeJoint.cpp ca. line 204. Also, since this body is connected to
         ground we can simply take the world pose of the link as initial transform of
         the mob. */
      Vec3 t, angles;
      Pose2PosAngles(t, angles, gzOutb->WorldPose());
      const OpenSim::CoordinateSet& coordinateSet = osimMob.joint->get_CoordinateSet();
      coordinateSet.get(0).setDefaultValue(angles[0]);
      coordinateSet.get(1).setDefaultValue(angles[1]);
      coordinateSet.get(2).setDefaultValue(angles[2]);
      coordinateSet.get(3).setDefaultValue(t[0]);
      coordinateSet.get(4).setDefaultValue(t[1]);
      coordinateSet.get(5).setDefaultValue(t[2]);

      OPENSIM_PHYSICS_DEBUG_ADD_MODEL(gzdbg << "  pose xyz, t = " << angles << ", " << t << std::endl;)
    }
    else // isAddedBaseMobilizer
    {
      // This mobilizer does correspond to one of the input joints.
      const bool isReversed = mob.isReversedFromJoint();

      gzJoint->GetPriv()->outboardLink = gzOutb;

      OPENSIM_PHYSICS_DEBUG_ADD_MODEL(gzdbg << "  joint = " << FormatName(gzJoint) << std::endl;)

      // This is guaranteed to be processed already because topological order.
      // It could be the ground body, though. In which case we have no body set because we don't own it (the model does).
      // TODO: Maybe write a member for OpensimLink which executes essentially the line below.

      OpenSim::Body *inb_body = gzInb ? &gzInb->GetPriv()->getOsimBody() : &this->dataPtr->osimModel.getGroundBody();
      OPENSIM_PHYSICS_DEBUG_ADD_MODEL(gzdbg << "    inb_osim_body = " << FormatName(inb_body) << std::endl;)
      if (!inb_body)
        gzthrow("The inboard link has no Opensim body reference!");

      // Find inboard and outboard frames for the mobilizer; these are
      // parent and child frames or the reverse.
      ignition::math::Pose3d parentAnchorPose = gzJoint->GetParentAnchorPose();
      ignition::math::Pose3d childAnchorPose  = gzJoint->GetChildAnchorPose();
      Transform Xbm = Pose2Transform(isReversed ? parentAnchorPose : childAnchorPose);
      Transform Xpf = Pose2Transform(isReversed ? childAnchorPose : parentAnchorPose);

      if (type == "ball")
      {
        osimMob.joint = JointFactory<OpenSim::BallJoint>(
          Xbm, Xpf, *inb_body, *osimMob.body);

//         Rotation defR_FM = isReversed
//             ? Rotation(~gzJoint->defxAB.R())
//             : gzJoint->defxAB.R();
//         ballJoint.setDefaultRotation(defR_FM);
//         mobod = ballJoint;

     }
      else if (type == "revolute")
      {
        // rotation from axis frame to child link frame
        // opensim assumes links are in child link frame, but gazebo
        // sdf 1.4 and earlier assumes joint axis are defined in model frame.
        // Use function Joint::GetAxisFrame() to remedy this situation.
        // Joint::GetAxisFrame() returns the frame joint axis is defined:
        // either model frame or child link frame.
        // opensim always assumes axis is specified in the child link frame.
        // \TODO: come up with a test case where we might need to
        // flip transform based on isReversed flag.
         UnitVec3 axis(
          Vector3ToVec3(
            gzJoint->AxisFrameOffset(0).RotateVector(
            gzJoint->LocalAxis(0))));

        OPENSIM_PHYSICS_DEBUG_ADD_MODEL(
          gzdbg << "    axis frame offset = " << gzJoint->AxisFrameOffset(0).Euler() << std::endl;
          gzdbg << "    local axis        = " << gzJoint->LocalAxis(0) << std::endl;

        )

        // Rotates the zAxis to align with the given axis.
        Rotation R_JZ(axis, SimTK::ZAxis);
        Xbm = Transform(Xbm.R() * R_JZ, Xbm.p());
        Xpf = Transform(Xpf.R() * R_JZ, Xpf.p());

        osimMob.joint = JointFactory<OpenSim::PinJoint>(
          Xbm, Xpf, *inb_body, *osimMob.body);
      }
      else if (type == "fixed")
      {
        osimMob.joint = JointFactory<OpenSim::WeldJoint>(
          Xbm, Xpf, *inb_body, *osimMob.body);
      }
#if 0
      if (type == "screw")
      {
        UnitVec3 axis(
          Vector3ToVec3(
            gzJoint->GetAxisFrameOffset(0).RotateVector(
            gzJoint->GetLocalAxis(0))));

        double pitch =
          dynamic_cast<physics::OpensimScrewJoint*>(gzJoint)->GetThreadPitch(0);

        if (math::equal(pitch, 0.0))
        {
          gzerr << "thread pitch should not be zero (joint is a slider?)"
                << " using pitch = 1.0e6\n";
          pitch = 1.0e6;
        }

        // Opensim's screw joint axis (both rotation and translation) is along Z
        Rotation R_JZ(axis, ZAxis);
        Transform X_IF(X_IF0.R()*R_JZ, X_IF0.p());
        Transform X_OM(X_OM0.R()*R_JZ, X_OM0.p());
        MobilizedBody::Screw screwJoint(
            parentMobod,      X_IF,
            massProps,        X_OM,
            -1.0/pitch,
            direction);
        mobod = screwJoint;

        gzdbg << "Setting limitForce[0] for [" << gzJoint->GetName() << "]\n";

        double low = gzJoint->GetLowerLimit(0u).Radian();
        double high = gzJoint->GetUpperLimit(0u).Radian();

        // initialize stop stiffness and dissipation from joint parameters
        gzJoint->limitForce[0] =
          Force::MobilityLinearStop(this->forces, mobod,
          SimTK::MobilizerQIndex(0), gzJoint->GetStopStiffness(0),
          gzJoint->GetStopDissipation(0), low, high);

        // gzdbg << "OpensimPhysics SetDamping ("
        //       << gzJoint->GetDampingCoefficient()
        //       << ")\n";
        // Create a damper for every joint even if damping coefficient
        // is zero.  This will allow user to change damping coefficients
        // on the fly.
        gzJoint->damper[0] =
          Force::MobilityLinearDamper(this->forces, mobod, 0,
                                   gzJoint->GetDamping(0));

        // add spring (stiffness proportional to mass)
        gzJoint->spring[0] =
          Force::MobilityLinearSpring(this->forces, mobod, 0,
            gzJoint->GetStiffness(0),
            gzJoint->GetSpringReferencePosition(0));
      }
      else if (type == "universal")
      {
        UnitVec3 axis1(Vector3ToVec3(
          gzJoint->GetAxisFrameOffset(0).RotateVector(
          gzJoint->GetLocalAxis(UniversalJoint<Joint>::AXIS_PARENT))));
        /// \TODO: check if this is right, or GetAxisFrameOffset(1) is needed.
        UnitVec3 axis2(Vector3ToVec3(
          gzJoint->GetAxisFrameOffset(0).RotateVector(
          gzJoint->GetLocalAxis(UniversalJoint<Joint>::AXIS_CHILD))));

        // Opensim's univeral joint is along axis1=Y and axis2=X
        // note X and Y are reversed because Opensim defines universal joint
        // rotation in body-fixed frames, whereas Gazebo/ODE uses space-fixed
        // frames.
        Rotation R_JF(axis1, XAxis, axis2, YAxis);
        Transform X_IF(X_IF0.R()*R_JF, X_IF0.p());
        Transform X_OM(X_OM0.R()*R_JF, X_OM0.p());
        MobilizedBody::Universal uJoint(
            parentMobod,      X_IF,
            massProps,        X_OM,
            direction);
        mobod = uJoint;

        for (unsigned int nj = 0; nj < 2; ++nj)
        {
          double low = gzJoint->GetLowerLimit(nj).Radian();
          double high = gzJoint->GetUpperLimit(nj).Radian();

          // initialize stop stiffness and dissipation from joint parameters
          gzJoint->limitForce[nj] =
            Force::MobilityLinearStop(this->forces, mobod,
            SimTK::MobilizerQIndex(nj), gzJoint->GetStopStiffness(nj),
            gzJoint->GetStopDissipation(nj), low, high);

          // gzdbg << "stop stiffness [" << gzJoint->GetStopStiffness(nj)
          //       << "] low [" << low
          //       << "] high [" << high
          //       << "]\n";

          // gzdbg << "OpensimPhysics SetDamping ("
          //       << gzJoint->GetDampingCoefficient()
          //       << ")\n";
          // Create a damper for every joint even if damping coefficient
          // is zero.  This will allow user to change damping coefficients
          // on the fly.
          gzJoint->damper[nj] =
            Force::MobilityLinearDamper(this->forces, mobod, nj,
                                     gzJoint->GetDamping(nj));
          // add spring (stiffness proportional to mass)
          gzJoint->spring[nj] =
            Force::MobilityLinearSpring(this->forces, mobod, nj,
              gzJoint->GetStiffness(nj),
              gzJoint->GetSpringReferencePosition(nj));
        }
      }
#endif
      else if (type == "prismatic")
      {
        UnitVec3 axis(Vector3ToVec3(
            gzJoint->AxisFrameOffset(0).RotateVector(
            gzJoint->LocalAxis(0))));

        // Simbody's slider is along X
//         Rotation R_JX(axis, XAxis);
//         Transform X_IF(X_IF0.R()*R_JX, X_IF0.p());
//         Transform X_OM(X_OM0.R()*R_JX, X_OM0.p());
//         MobilizedBody::Slider sliderJoint(
//             parentMobod,      X_IF,
//             massProps,              X_OM,
//             direction);
//         mobod = sliderJoint;
//
//         double low = gzJoint->GetLowerLimit(0u).Radian();
//         double high = gzJoint->GetUpperLimit(0u).Radian();
//
//         // initialize stop stiffness and dissipation from joint parameters
//         gzJoint->limitForce[0] =
//           Force::MobilityLinearStop(this->forces, mobod,
//           SimTK::MobilizerQIndex(0), gzJoint->GetStopStiffness(0),
//           gzJoint->GetStopDissipation(0), low, high);
//
//         // Create a damper for every joint even if damping coefficient
//         // is zero.  This will allow user to change damping coefficients
//         // on the fly.
//         gzJoint->damper[0] =
//           Force::MobilityLinearDamper(this->forces, mobod, 0,
//                                    gzJoint->GetDamping(0));
//
//         // add spring (stiffness proportional to mass)
//         gzJoint->spring[0] =
//           Force::MobilityLinearSpring(this->forces, mobod, 0,
//             gzJoint->GetStiffness(0),
//             gzJoint->GetSpringReferencePosition(0));

        // Rotates the x axis to align with the given axis.
        Rotation R_JX(axis, SimTK::XAxis);
        Xbm = Transform(Xbm.R() * R_JX, Xbm.p());
        Xpf = Transform(Xpf.R() * R_JX, Xpf.p());

        osimMob.joint = JointFactory<OpenSim::SliderJoint>(
          Xbm, Xpf, *inb_body, *osimMob.body);
      }
      else // joint type
      {
        gzthrow("Opensim joint type [" << type << "] not implemented.");
      }

      this->dataPtr->osimModel.addBody(osimMob.body.get());

      gzJoint->dataPtr->InitializeJointForces();

      gzJoint->dataPtr->osimRepresentationInitialized = true;
      gzJoint->dataPtr->mob_index = gzOutbPriv->mobs.size()-1;
    } // isAddedBaseMobilizer

    if (!isSlave)
    {
      // Collision geometry is only added to the master body.
      this->AddLinkCollisions(*gzOutb, *gzOutbPriv->mobs[gzOutbPriv->master_index].body, modelClique);
    }
  } // for num mobilizers
}


/////////////////////////////////////////////////
void OpensimPhysics::AddLinkCollisions(
  const OpensimLink &_link,
  OpenSim::Body &_body,
  SimTK::ContactCliqueId _modelClique)
{
//   bool addModelClique = _modelClique.isValid() && !_link->GetSelfCollide();

#define INVOKE_ADDITION_OF_COLLISION_SHAPE_TO_BODY(type) \
  auto p = boost::dynamic_pointer_cast<type>(sc->GetShape()); \
  p->AddToBody(this->dataPtr.get(), _body); \
  p->osimGeometry->setUserData(sc.get()); \
  sc->osimGeometry = p->osimGeometry.get();

/* Here we ask every shape instance to add itself to the OpenSim model.
 * For easier access to the geometry we also keep a pointer to it
 * in the Collision instance.
 * Very important: we set the user data pointer of the OpenSim contact
 * geometry to point to the gazebo collision object for later retrieval
 * when forwarning contact information. The pointer must be set prior to
 * rebuilding the system for the pointer to appear later in the ContactData
 * struct (see implementation of OpenSim::HuntCrossleyForce). */

  try {
    const Collision_V &collisions =  _link.GetCollisions();
    for (CollisionPtr collision_base : collisions)
    {
      OpensimCollisionPtr sc =
        boost::dynamic_pointer_cast<physics::OpensimCollision>(collision_base);
      GZ_ASSERT(sc != nullptr, "Expected OpensimCollision instance but got something else.");

      switch (sc->GetShapeType() & (~physics::Entity::SHAPE))
      {
        case physics::Entity::PLANE_SHAPE:
        {
          INVOKE_ADDITION_OF_COLLISION_SHAPE_TO_BODY(OpensimPlaneShape)
        }
        break;
        case physics::Entity::SPHERE_SHAPE:
        {
          INVOKE_ADDITION_OF_COLLISION_SHAPE_TO_BODY(OpensimSphereShape)
        }
        break;
        case physics::Entity::MESH_SHAPE:
        {
          INVOKE_ADDITION_OF_COLLISION_SHAPE_TO_BODY(OpensimMeshShape)
        }
        break;
        case physics::Entity::CYLINDER_SHAPE:
        {
          INVOKE_ADDITION_OF_COLLISION_SHAPE_TO_BODY(OpensimCylinderShape)
        }
        break;
        case physics::Entity::BOX_SHAPE:
        {
          INVOKE_ADDITION_OF_COLLISION_SHAPE_TO_BODY(OpensimBoxShape)
        }
        break;
        default:
          gzerr << "Collision type [" << ShapeTypeStr(sc->GetShapeType())
                << "] unimplemented\n";
          break;
      }
    }
    // Add ContactForce instances for all compatible ContactSurfaces
    this->dataPtr->contactForceManager.AddToContactForceSet(collisions);
  }
  catch(SimTK::Exception::Base& ex)
  {
    gzerr << "OpensimPhysics::AddLinkCollisions: " << ex.what() << std::endl;
    
    // gzserver's loading world takes less time than gzbridge's set up, so we 
    // wait a bit of time.
    if (this->waitForConnectionOnError){
      this->errorPublisher->WaitForConnection(
        gazebo::common::Time(error::WAIT_GZBRIDGE_SECONDS));
      this->waitForConnectionOnError = false;
    }    
    msgs::Error errorMsg;
    errorMsg.set_id(error::OPENSIM_COLLISION_MESH);
    errorMsg.set_filename("OpensimPhysics.cc");
    errorMsg.set_description(ex.what());
    this->errorPublisher->Publish(errorMsg);
  }
}

/////////////////////////////////////////////////
namespace gazebo { namespace physics {
class RemovingModels
{
  OpensimPhysics &engine;
  OpenSim::Model &osimModel;
  OpenSim::ForceSet &osimForces;
public:
  RemovingModels(OpensimPhysics &_engine)
    : engine(_engine),
      osimModel(_engine.GetPriv()->osimModel),
      osimForces(_engine.GetPriv()->osimModel.updForceSet())
  {
  }

  void RemoveTheModelHierarchy(Model &_root)
  {
    ForTheModelHierarchy(_root,
      [this](Model &_model)
      {
        RemoveModel(_model);
      });
  }

  void RemoveModel(Model &_base_model)
  {
    OpensimModel &model = static_cast<OpensimModel&>(_base_model);
    const physics::Link_V &links = model.GetLinks();
    for (auto link_base : links)
    {
      OpensimLinkPtr link = boost::dynamic_pointer_cast<OpensimLink>(link_base);
      GZ_ASSERT(link != nullptr, "There should only be links created by OpensimPhysics");

      Collision_V collisions = link_base->GetCollisions();
      engine.RemoveCollisionsFromOpensimSystem(collisions);

      link->GetPriv()->master_index = -1;
      for (Mob &mob : link->GetPriv()->mobs)
      {
        osimModel.updBodySet().remove(mob.body.get());
        osimModel.updJointSet().remove(mob.joint.get());
        if (mob.slave_weld)
          osimModel.updConstraintSet().remove(mob.slave_weld.get());
      }
    }

    const physics::Joint_V &joints = model.GetJoints();
    for (auto joint_base : joints)
    {
      OpensimJointPtr joint = boost::dynamic_pointer_cast<OpensimJoint>(joint_base);
      GZ_ASSERT(joint != nullptr, "There should only be joints created by OpensimPhysics");
      OpensimJointPrivate *priv = joint->GetPriv();

      for (std::size_t i = 0; i<priv->isForceAddedToModel.size(); ++i)
      {
        if (!priv->isForceAddedToModel[i]) continue;

        Remove(&priv->externalCoordinateForce[i]);
        Remove(&priv->springCoordinateForce[i]);
        Remove(&priv->coordinateLimitForce[i]);
        priv->isForceAddedToModel[i] = false;
      }
    }
    for (auto &f : model.owned_osim_forces)
    {
      Remove(f.get());
    }
    for (auto muscle : model.GetMuscles())
    {
      muscle->RemoveFrom(osimModel);
    }
  }


  inline void Remove(OpenSim::Force* f)
  {
    remove<OpenSim::Force>(osimForces, f);
  }

};
}} // namespaces

//////////////////////////////////////////////////
void OpensimPhysics::RemoveModelHierarchyFromOpensimSystem(OpensimModel &_model)
{
/* Called via OpensimModel::Fini. For consistency reasons
 * is handled in opensim physics class. Called before
 * Fini on joints and links. */
  OPENSIM_PHYSICS_DEBUG1(gzdbg << __FUNCTION__ << " on " << _model.GetName() << std::endl;)
  boost::recursive_mutex::scoped_lock lock(
  *this->GetPhysicsUpdateMutex());
  this->opensimPhysicsInitialized = false;

  RemovingModels removing_models(*this);
  removing_models.RemoveTheModelHierarchy(_model);

  if (!this->opensimPhysicsFirstStart)
  {
    this->SystemUpdateAfterTopologyChange();
    this->opensimPhysicsInitialized = true;
  }
}

//////////////////////////////////////////////////
void OpensimPhysics::RemoveCollisionsFromOpensimSystem(const Collision_V &collisions)
{
  #define FETCH_THE_POINTER_TO_THE_OPENSIM_GEOMETRY(type) \
    auto p = boost::dynamic_pointer_cast<type>(sc->GetShape()); \
    geom = p->osimGeometry.get(); \
    this->dataPtr->osimModel.updContactGeometrySet().remove(geom);

  for (auto sc : collisions)
  {
    OpenSim::ContactGeometry *geom = nullptr;

    switch (sc->GetShapeType() & (~physics::Entity::SHAPE))
    {
      case physics::Entity::PLANE_SHAPE:
      {
        FETCH_THE_POINTER_TO_THE_OPENSIM_GEOMETRY(OpensimPlaneShape)
      }
      break;
      case physics::Entity::SPHERE_SHAPE:
      {
        FETCH_THE_POINTER_TO_THE_OPENSIM_GEOMETRY(OpensimSphereShape)
      }
      break;
      case physics::Entity::MESH_SHAPE:
      {
        FETCH_THE_POINTER_TO_THE_OPENSIM_GEOMETRY(OpensimMeshShape)
      }
      break;
      case physics::Entity::CYLINDER_SHAPE:
      {
        FETCH_THE_POINTER_TO_THE_OPENSIM_GEOMETRY(OpensimCylinderShape)
      }
      break;
      case physics::Entity::BOX_SHAPE:
      {
        FETCH_THE_POINTER_TO_THE_OPENSIM_GEOMETRY(OpensimBoxShape)
      }
      break;
      default:
        gzerr << "Collision type [" << ShapeTypeStr(sc->GetShapeType())
              << "] unimplemented\n";
        break;
    }
  }

  this->dataPtr->contactForceManager.RemoveFromContactForceSet(collisions);
}


//////////////////////////////////////////////////
namespace gazebo { namespace physics {
namespace MultibodyGraphBuilding
{

/////////////////////////////////////////////////
std::string GetTypeString(physics::Base::EntityType _type)
{
  if (_type & physics::Base::BALL_JOINT)
    return "ball";
  else if (_type & physics::Base::HINGE2_JOINT)
      return "revolute2";
  else if (_type & physics::Base::HINGE_JOINT)
      return "revolute";
  else if (_type & physics::Base::SLIDER_JOINT)
      return "prismatic";
  else if (_type & physics::Base::SCREW_JOINT)
      return "screw";
  else if (_type & physics::Base::UNIVERSAL_JOINT)
      return "universal";
  else if (_type & physics::Base::FIXED_JOINT)
      return "fixed";

  gzerr << "Unrecognized joint type\n";
  return "UNRECOGNIZED";
}

/////////////////////////////////////////////////
std::string GetTypeString(unsigned int _type)
{
  return GetTypeString(physics::Base::EntityType(_type));
}

/////////////////////////////////////////////////
std::string GetUniqueName(const Link &link)
{
  // Even scoped name is not always unique within a hierarchy.
  // But maybe we can use addresses, rendered as string.
  // That should be unique. Fortunately, its use is restricted
  // the graph building stage.

  //return link.GetScopedName();
  char addr_str[16];
  std::snprintf(addr_str, 16, "%p", (void*)&link);
  return addr_str;
}

/////////////////////////////////////////////////
std::string GetUniqueName(const Joint &joint)
{
  return joint.GetScopedName();
}

/////////////////////////////////////////////////
std::string GetUniqueNameOfActuallyJointLink(const LinkPtr link)
{
  // We could link to the world, in case the GetParent()/GetChild()
  // pointers will be null. We need to deal with this.
  // Moreover we could link to a static model. Currently there are
  // no bodies generated for links of static models. So we have to
  // deal with these links as well.
  if (link == nullptr)
    return "world";
  else
  {
    GZ_ASSERT(link->GetModel() != nullptr, "Links should have models!");
    if (link->GetModel()->IsStatic())
      return "world";
    else
      return GetUniqueName(*link);
  }
}

/////////////////////////////////////////////////
void AddLinksToMultibodyGraph(
  SimTK::MultibodyGraphMaker& _mbgraph, const OpensimModel &_model)
{
  const physics::Link_V &links = _model.GetLinks();
  for (const LinkPtr &base_link : links)
  {
    OpensimLinkPtr link = boost::dynamic_pointer_cast<OpensimLink>(base_link);
    assert(link); // Could we get served with  Links that were not created by our physics engine?
    _mbgraph.addBody(GetUniqueName(*link), link->GetInertial()->Mass(),
                      false /*link->mustBeBaseLink*/, link.get());
    gzdbg << "Graphbuilder:" << "+ " << GetUniqueName(*link) << std::endl;
  }
}

/////////////////////////////////////////////////
void AddJointsToMultibodyGraph(
  SimTK::MultibodyGraphMaker& _mbgraph, const OpensimModel &_model)
{
  const physics::Joint_V &joints = _model.GetJoints();
  for (const JointPtr &base_joint : joints)
  {
    OpensimJointPtr joint = boost::dynamic_pointer_cast<OpensimJoint>(base_joint);
    assert(joint);

    if (!(joint->GetChild() || joint->GetParent()))
    {
      gzmsg << "Joint \"" << joint->GetScopedName() << "\" with no parent nor child is ignored!" << std::endl;
      continue;
    }

    const std::string parent_name = GetUniqueNameOfActuallyJointLink(joint->GetParent());
    const std::string child_name = GetUniqueNameOfActuallyJointLink(joint->GetChild());

    _mbgraph.addJoint(
        GetUniqueName(*joint), GetTypeString(joint->GetType()),
        parent_name,
        child_name,
        joint->GetPriv()->mustBreakLoopHere,
        joint.get());
    gzdbg << "Graphbuilder:" << "+ " << parent_name << "<-" << child_name << std::endl;
  }
}

/////////////////////////////////////////////////
void AddToMultibodyGraphRecursive(
        SimTK::MultibodyGraphMaker& _mbgraph,
        const OpensimModel &_model)
{
  ForTheConstModelHierarchy(_model,
    [&_mbgraph](const Model &_local_model) {
      if (!_local_model.IsStatic())
      {
        AddLinksToMultibodyGraph(_mbgraph, static_cast<const OpensimModel&>(_local_model));
      }
    });

  ForTheConstModelHierarchy(_model,
    [&_mbgraph](const Model &_local_model) {
      if (!_local_model.IsStatic())
      {
        AddJointsToMultibodyGraph(_mbgraph, static_cast<const OpensimModel&>(_local_model));
      }
    });
}

/////////////////////////////////////////////////
/// \brief CREATE MULTIBODY GRAPH
/// Define Gazebo joint types, then use links and joints in the
/// given model to construct a reasonable spanning-tree-plus-constraints
/// multibody graph to represent that model. An exception will be
/// thrown if this fails.  Note that this step is not Opensim dependent.
/// \param[in] _model Model loaded by Gazebo parsing SDF.
SimTK::MultibodyGraphMaker CreateMultibodyGraph(const OpensimModel &_model)
{
  SimTK::MultibodyGraphMaker mbgraph;
  // Step 1: Tell MultibodyGraphMaker about joints it should know about.
  // Note: "weld" and "free" are always predefined at 0 and 6 dofs, resp.
  //                  Gazebo name  #dofs     Opensim equivalent
  mbgraph.addJointType(GetTypeString(physics::Base::HINGE_JOINT),  1);
  mbgraph.addJointType(GetTypeString(physics::Base::HINGE2_JOINT), 2);
  mbgraph.addJointType(GetTypeString(physics::Base::SLIDER_JOINT), 1);
  mbgraph.addJointType(GetTypeString(physics::Base::UNIVERSAL_JOINT), 2);
  mbgraph.addJointType(GetTypeString(physics::Base::SCREW_JOINT), 1);
  mbgraph.addJointType(GetTypeString(physics::Base::FIXED_JOINT), 0);
  // Opensim has a Ball constraint that is a good choice if you need to
  // break a loop at a ball joint.
  // _mbgraph.addJointType(GetTypeString(physics::Base::BALL_JOINT), 3, true);
  // skip loop joints for now
  mbgraph.addJointType(GetTypeString(physics::Base::BALL_JOINT), 3, false);
  // Step 2: Tell it about all the links we read from the input file,
  // starting with world, and provide a reference pointer.
  mbgraph.addBody("world", SimTK::Infinity, false);

  MultibodyGraphBuilding::AddToMultibodyGraphRecursive(mbgraph, _model);

  // Setp 4. Generate the multibody graph.
  mbgraph.generateGraph();
  return mbgraph; // RVO??
}


} // namespace MultibodyGraphBuilding
}} // namespaces

/////////////////////////////////////////////////
void OpensimPhysics::SetSeed(uint32_t /*_seed*/)
{
  gzerr << "OpensimPhysics::SetSeed not implemented\n";
}

/////////////////////////////////////////////////
std::string OpensimPhysics::GetType() const
{
  return "opensim";
}

//////////////////////////////////////////////////
boost::any OpensimPhysics::GetParam(const std::string &_key) const
{
  boost::any value;
  this->GetParam(_key, value);
  return value;
}

//////////////////////////////////////////////////
bool OpensimPhysics::GetParam(const std::string &_key, boost::any &_value) const
{
  if (_key == "solver_type")
  {
    _value = std::string("Spatial Algebra and Elastic Foundation");
  }
  else if (_key == "integrator" || _key == "integrator_type")
  {
    _value = this->dataPtr->integratorType;
  }
  else if (_key == "accuracy" || _key == "integrator_accuracy")
  {
    if (this->dataPtr->integrator)
      _value = this->dataPtr->integrator->getAccuracyInUse();
    else
      _value = this->dataPtr->integratorAccuracy;
  }
  else if (_key == "min_step_size")
  {
    _value = this->dataPtr->integratorMinStepSize;
  }
  else if (_key == "stiffness")
  {
    _value = this->globalOverrideSurfaceParams.stiffness;
  }
  else if (_key == "dissipation")
  {
    _value = this->globalOverrideSurfaceParams.dissipation;
  }
  else if (_key == "transitionVelocity" || _key == "transition_velocity" )
  {
    _value = this->globalOverrideSurfaceParams.transitionVelocity;
  }
  else if (_key == "staticFriction" || _key == "static_friction")
  {
    _value = this->globalOverrideSurfaceParams.staticFriction;
  }
  else if (_key == "dynamicFriction" || _key == "dynamic_friction")
  {
    _value = this->globalOverrideSurfaceParams.dynamicFriction;
  }
  else if (_key == "viscousFriction" || _key == "viscous_friction")
  {
    _value = this->globalOverrideSurfaceParams.viscousFriction;
  }
  else
  {
    return PhysicsEngine::GetParam(_key, _value);
  }
  return true;
}

//////////////////////////////////////////////////
bool OpensimPhysics::SetParam(const std::string &_key, const boost::any &_value)
{
  try
  {
    boost::recursive_mutex::scoped_lock lock(
    *this->GetPhysicsUpdateMutex());

    if (_key == "integrator_accuracy")
    {
      this->dataPtr->integratorAccuracy = boost::any_cast<double>(_value);
      this->dataPtr->integrator->setAccuracy(boost::any_cast<double>(_value));
    }
    else if (_key == "min_step_size")
    {
      this->dataPtr->integratorMinStepSize = boost::any_cast<double>(_value);
      this->dataPtr->integrator->setMinimumStepSize(boost::any_cast<double>(_value));
    }
    else if (_key == "stiffness")
    {
      this->globalOverrideSurfaceParams.stiffness = boost::any_cast<double>(_value);
    }
    else if (_key == "stiffness")
    {
      this->globalOverrideSurfaceParams.stiffness = boost::any_cast<double>(_value);
    }
    else if (_key == "dissipation")
    {
      this->globalOverrideSurfaceParams.dissipation = boost::any_cast<double>(_value);
    }
    else if (_key == "transitionVelocity" || _key == "transition_velocity")
    {
      this->globalOverrideSurfaceParams.transitionVelocity = boost::any_cast<double>(_value);
    }
    else if (_key == "staticFriction" || _key == "static_friction")
    {
      this->globalOverrideSurfaceParams.staticFriction = boost::any_cast<double>(_value);
    }
    else if (_key == "dynamicFriction" || _key == "dynamic_friction")
    {
      this->globalOverrideSurfaceParams.dynamicFriction = boost::any_cast<double>(_value);
    }
    else if (_key == "viscousFriction" || _key == "viscous_friction")
    {
      this->globalOverrideSurfaceParams.viscousFriction = boost::any_cast<double>(_value);
    }
    else
    {
      // Parameter not supported
      gzwarn << "OpensimPhysics::SetParam(" << _key << "): Parameter not supported!" << std::endl;
      return false;
    }
  }
  catch(boost::bad_any_cast &e)
  {
     gzerr << "OpensimPhysics::SetParam(" << _key << ") boost::any_cast error: "
           << e.what() << std::endl;
     return false;
  }
  return true;
}

#ifdef OPENSIM_USE_DEBUG_DRAWER
DebugDrawer* OpensimPhysics::GetDebugDrawer()
{
  return this->GetPriv()->debugDrawer;
}
#endif