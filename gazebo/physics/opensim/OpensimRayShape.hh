/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#ifndef _OPENSIM_RAY_SHAPE_HH_
#define _OPENSIM_RAY_SHAPE_HH_

#include <string>
#include "gazebo/physics/RayShape.hh"
#include "gazebo/util/system.hh"

namespace gazebo
{
namespace physics
{
/// \ingroup gazebo_physics
/// \addtogroup gazebo_physics_opensim Opensim Physics
/// \{

/// \brief Ray shape for opensim
class GZ_PHYSICS_VISIBLE OpensimRayShape : public RayShape
{
  /// \brief Constructor.
  /// \param[in] _physicsEngine Pointer to the physics engine.
public:
  OpensimRayShape(PhysicsEnginePtr _physicsEngine);

  /// \brief Constructor
  /// \param[in] _collision Collision the ray is attached to.
public:
  OpensimRayShape(CollisionPtr _collision);

  /// \brief Destructor
public:
  virtual ~OpensimRayShape() override;

  // Documentation inherited
public:
  virtual void Update() override;

  // Documentation inherited
public:
  virtual void GetIntersection(double &_dist, std::string &_entity) override;

  // Documentation inherited
public:
  virtual void SetPoints(const ignition::math::Vector3d &_posStart,
                         const ignition::math::Vector3d &_posEnd) override;

  /// \brief Pointer to the physics engine.
private:
  OpensimPhysicsPtr physicsEngine;
};
/// \}
}
}
#endif
