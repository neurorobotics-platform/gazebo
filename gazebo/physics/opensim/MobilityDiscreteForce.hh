/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#ifndef OPENSIM_MobilityDiscreteForce_H_
#define OPENSIM_MobilityDiscreteForce_H_

#include "OpenSim/Common/Property.h"
#include "OpenSim/Common/Set.h"
#include "OpenSim/Simulation/Model/Force.h"

namespace OpenSim
{

class Coordinate;
class Model;

/* This class implements the bare minimal functionality to
 * introduce a  SimTK::Force::MobilityDiscreteForce into the system.
 * I skipped most OpenSim things like properties. Accepting a
 * Coordinate directly rather than by its name is also "non-standard"
 */

class OSIMSIMULATION_API MobilityDiscreteForce : public Force
{
        OpenSim_DECLARE_CONCRETE_OBJECT(MobilityDiscreteForce, Force) public :
            //==============================================================================
            // PUBLIC METHODS
            //==============================================================================
            MobilityDiscreteForce();
        /*
        It would be nice to precisely control what can happen to this thing. Unfortunately
        the OpenSim_DECLARE_CONCRETE_OBJECT requires copy ctors. Perhaps we should just not
        use this macro?*/
        //         MobilityDiscreteForce(const MobilityDiscreteForce &) = delete;
        //         MobilityDiscreteForce& operator=(const MobilityDiscreteForce &) = delete;

        void setForce(double force_value);
        double getForce() const;
        void setCoordinate(Coordinate *coord);
        Coordinate *getCoordinate();

      protected:
        /** Setup this CoordinateLimitForce as part of the model.
            This were the existence of the coordinate to limit is checked. */
        void connectToModel(Model &aModel) OVERRIDE_11;
        /** Create the underlying Force that is part of the multibodysystem. */
        void addToSystem(SimTK::MultibodySystem &system) const OVERRIDE_11;

      private:
        SimTK::ForceIndex _simtk_force_idx;
        double _force_value;
        Coordinate *_coord;
        //==============================================================================
}; // END of class MobilityDiscreteForce
//==============================================================================
}
#endif // OPENSIM_MobilityDiscreteForce_H_
