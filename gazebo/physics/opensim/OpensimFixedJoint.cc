/*
 * Copyright (C) 2015-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#include "gazebo/common/Assert.hh"
#include "gazebo/common/Console.hh"
#include "gazebo/common/Exception.hh"

#include "gazebo/physics/Model.hh"
#include "gazebo/physics/opensim/OpensimLink.hh"
#include "gazebo/physics/opensim/OpensimPhysics.hh"
#include "gazebo/physics/opensim/OpensimFixedJoint.hh"
#include "gazebo/physics/opensim/OpensimPhysicsPrivate.hh"

using namespace gazebo;
using namespace physics;

//////////////////////////////////////////////////
OpensimFixedJoint::OpensimFixedJoint(BasePtr _parent, OpensimPhysics &_engine)
    : FixedJoint<OpensimJoint>(_parent)
{
  this->dataPtr = boost::make_shared<OpensimJointPrivate>(this, &_engine);
}

//////////////////////////////////////////////////
OpensimFixedJoint::~OpensimFixedJoint()
{
}

//////////////////////////////////////////////////
void OpensimFixedJoint::Load(sdf::ElementPtr _sdf)
{
  FixedJoint<OpensimJoint>::Load(_sdf);
}

// //////////////////////////////////////////////////
// void OpensimFixedJoint::SetVelocity(unsigned int /*_index*/, double /*_angle*/)
// {
//   gzwarn << "OpensimFixedJoint: called method "
//          << "SetVelocity that is not valid for joints of type fixed.\n";
// }
//
// //////////////////////////////////////////////////
// double OpensimFixedJoint::GetVelocity(unsigned int /*index*/) const
// {
//   gzwarn << "OpensimFixedJoint: called method "
//          << "GetVelocity that is not valid for joints of type fixed.\n";
//   return 0.0;
// }
//
// //////////////////////////////////////////////////
// void OpensimFixedJoint::SetForceImpl(unsigned int /*_index*/,
//                                      double /*_torque*/)
// {
//   gzwarn << "OpensimFixedJoint: called method "
//          << "SetForceImpl that is not valid for joints of type fixed.\n";
// }
//
// //////////////////////////////////////////////////
// ignition::math::Vector3d OpensimFixedJoint::GetGlobalAxis(unsigned int /*index*/) const
// {
//   gzwarn << "OpensimFixedJoint: called method "
//          << "GetGlobalAxis that is not valid for joints of type fixed.\n";
//   return ignition::math::Vector3d();
// }
//
// //////////////////////////////////////////////////
// ignition::math::Angle OpensimFixedJoint::GetAngleImpl(unsigned int /*_index*/) const
// {
//   gzwarn << "OpensimFixedJoint: called method "
//          << "GetAngleImpl that is not valid for joints of type fixed.\n";
//   return mignition::math::Angle();
// }

//////////////////////////////////////////////////
bool OpensimFixedJoint::CheckValidAxisAndDebugOut(unsigned int _axis, const char *_where) const
{
  return false;
}