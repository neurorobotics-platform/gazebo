/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#include "gazebo/physics/opensim/OpensimPlaneShape.hh"
#include "gazebo/physics/opensim/OpensimTypes.hh"
#include "gazebo/physics/opensim/OpensimCollision.hh"

#include "gazebo/physics/World.hh"
#include "gazebo/physics/opensim/OpensimPhysics.hh"
#include "OpensimLink.hh"
#include "OpensimPhysicsPrivate.hh"

#include "gazebo/common/Console.hh"

using namespace gazebo;
using namespace physics;

/////////////////////////////////////////////////
OpensimPlaneShape::OpensimPlaneShape(CollisionPtr _parent)
    : PlaneShape(_parent) /*, deferShapeCreation(true)*/
{
  OPENSIM_PHYSICS_LOG_CALL_ON(this->GetScopedName());
}

/////////////////////////////////////////////////
OpensimPlaneShape::~OpensimPlaneShape()
{
  OPENSIM_PHYSICS_LOG_CALL_ON(this->GetScopedName());
}

/////////////////////////////////////////////////
void OpensimPlaneShape::SetAltitude(const ignition::math::Vector3d &_pos)
{
  PlaneShape::SetAltitude(_pos);
}

/////////////////////////////////////////////////
void OpensimPlaneShape::AddToBody(OpensimPhysicsPrivate *_engine_priv, OpenSim::Body &_body)
{
  OpensimCollisionPtr collision = boost::dynamic_pointer_cast<OpensimCollision>(this->collisionParent);

  Transform X_LC =
      Pose2Transform(
          collision->RelativePose());

  // rotation of the plane so that its local x-axis coincides with the specified normal
  SimTK::Rotation R_XN(-SimTK::UnitVec3(Vector3ToVec3(this->Normal())), SimTK::XAxis);
  X_LC = X_LC * SimTK::Transform(R_XN);

  // convert to position/euler representation
  SimTK::Vec3 pos = X_LC.p();
  SimTK::Vec3 euler = X_LC.R().convertRotationToBodyFixedXYZ();

  std::string osim_name = _engine_priv->CreateUniqueOsimNameFor(collision.get());
  this->osimGeometry = boost::make_shared<OpenSim::ContactHalfSpace>(pos, euler, _body, osim_name);

  _engine_priv->osimModel.addContactGeometry(this->osimGeometry.get());

  gzdbg << "Created new OpenSim::ContactPlane: normal = " << this->Normal()
        << ", collision = " << collision->GetName()
        << ", to body = " << _body.getName() << std::endl;
}

/////////////////////////////////////////////////
// std::string OpensimPlaneShape::GetOsimName() const
// {
//   GZ_ASSERT(this->osimGeometry != nullptr, "Pointer to opensim contact geometry not initialized!")
//   return this->osimGeometry->getName();
// }

/////////////////////////////////////////////////
void OpensimPlaneShape::CreatePlane()
{
  /* This functions is called at really inconvenient times
   * when the physics engine is not properly initialized yet.
   * So we do the creation of the geometry in AddToBody instead.
   */

  // TODO deal with parameter changes, i.e. changes of the normal which will result in a call to CreatePlane!
  //OPENSIM_PHYSICS_DEBUG1(gzdbg << __FUNCTION__ << " on " << this->GetName() << std::endl;)
  PlaneShape::CreatePlane();
}
