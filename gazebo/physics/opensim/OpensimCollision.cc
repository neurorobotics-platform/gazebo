/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#include "gazebo/common/Console.hh"

#include "gazebo/physics/opensim/opensim_inc.h"
#include "gazebo/physics/opensim/OpensimTypes.hh"
#include "gazebo/physics/opensim/OpensimCollision.hh"
#include "gazebo/physics/SurfaceParams.hh"
#include "gazebo/physics/opensim/OpensimPhysicsPrivate.hh"

#include <limits>

using namespace gazebo;
using namespace physics;

//////////////////////////////////////////////////
OpensimCollision::SurfaceParameters::SurfaceParameters() : stiffness(std::numeric_limits<double>::quiet_NaN()),
                                                           dissipation(std::numeric_limits<double>::quiet_NaN()),
                                                           transitionVelocity(std::numeric_limits<double>::quiet_NaN()), // in m/s, this is like slowly walking.
                                                           staticFriction(std::numeric_limits<double>::quiet_NaN()),
                                                           dynamicFriction(std::numeric_limits<double>::quiet_NaN()),
                                                           viscousFriction(std::numeric_limits<double>::quiet_NaN())
{
  // Proper values are assigned in the load function.
  // The SDF spec provides defaults if nothing is specified in the model.
}

//////////////////////////////////////////////////
OpensimCollision::OpensimCollision(LinkPtr _parent)
    : Collision(_parent)
{
  this->osimGeometry = NULL;
  this->surface.reset(new SurfaceParams()); // member of the base class
}

//////////////////////////////////////////////////
OpensimCollision::~OpensimCollision()
{
  OPENSIM_PHYSICS_LOG_CALL_ON(this->GetScopedName());
  this->osimGeometry = NULL;
}

//////////////////////////////////////////////////
void OpensimCollision::Load(sdf::ElementPtr _sdf)
{
  Collision::Load(_sdf);

  OpensimLink *link_ = static_cast<OpensimLink *>(this->GetLink().get());
  GZ_ASSERT(link != nullptr, "Collision object should be attached to a link at this point");
  OpensimPhysics *engine = link_->GetPriv()->engine;

  // After the call to the base class so we know the name.
  OPENSIM_PHYSICS_LOG_CALL_ON(this->GetScopedName());

  auto &sp = this->surfaceParameters;
  sp = engine->globalOverrideSurfaceParams; // Initialize with global settings

  // With the proper sdf spec in the sdformat lib, obtaining these elements
  // should never fail (unless out of memory ofc.). If not given in the model
  // default values will be taken from the sdf, i.e. the elements will be
  // created in the sdf tree in memory.
  auto surface_elem = _sdf->GetElement("surface");
  if (surface_elem == nullptr)
    gzthrow("Cannot get the surface element from the sdf.");

  auto friction = surface_elem->GetElement("friction");
  if (friction == nullptr)
    gzthrow("Cannot get the surface/friction element from the sdf.");

  auto opensim = friction->GetElement("opensim");
  if (opensim == nullptr)
    gzthrow("Cannot get the surface/friction/opensim element from the sdf.");

  // Here we check if the element is actually given in the model.
  // If not we do nothing and just use the global default.
  auto AssignIfHas = [](sdf::ElementPtr &el, const char *sub_el_name, double &dst) {
    if (el->HasElement(sub_el_name))
      dst = el->Get<double>(sub_el_name);
  };

  AssignIfHas(opensim, "static_friction", sp.staticFriction);
  AssignIfHas(opensim, "dynamic_friction", sp.dynamicFriction);
  AssignIfHas(opensim, "viscous_friction", sp.viscousFriction);
  AssignIfHas(opensim, "stiction_transition_velocity", sp.transitionVelocity);

  auto contact = surface_elem->GetElement("contact");
  if (contact == nullptr)
    gzthrow("Cannot get the surface/contact element from the sdf.");

  opensim = contact->GetElement("opensim");
  if (opensim == nullptr)
    gzthrow("Cannot get the surface/contact/opensim element from the sdf.");

  bool ok = false;
  if (!opensim->HasElement("stiffness") &&
      contact->HasElement("poissons_ratio") &&
      contact->HasElement("elastic_modulus"))
  {
    double poissons_ratio = contact->Get<double>("poissons_ratio");
    double elastic_modulus = contact->Get<double>("elastic_modulus");
    double tmp = (1.0 - poissons_ratio * poissons_ratio);
    if (tmp > 0.)
    {
      sp.stiffness = elastic_modulus / tmp;
      gzmsg << "Surface stiffness (" << sp.stiffness << ") of " << this->GetScopedName() << " computed from poissons_ratio and elastic_modulus" << std::endl;
      ok = true;
    }
    else
    {
      gzmsg << "Attempt to obtain surface stiffness of " << this->GetScopedName() << " from poissons_ratio and elastic_modulus but poissons_ratio (" << poissons_ratio << ") not within allowed range [0., 1.)" << std::endl;
    }
  }
  if (!ok) // get default
    AssignIfHas(opensim, "stiffness", sp.stiffness);
  AssignIfHas(opensim, "dissipation", sp.dissipation);
}

//////////////////////////////////////////////////
void OpensimCollision::Init()
{
  OPENSIM_PHYSICS_LOG_CALL_ON(this->GetScopedName());
  Collision::Init();
}

//////////////////////////////////////////////////
void OpensimCollision::OnPoseChange()
{
  // ignition::math::Pose3d pose = this->GetRelativePose();
  // OpensimLink *bbody = static_cast<OpensimLink*>(this->body);

  // bbody->SetCollisionRelativePose(this, pose);
}

//////////////////////////////////////////////////
void OpensimCollision::SetCategoryBits(unsigned int /*_bits*/)
{
}

//////////////////////////////////////////////////
void OpensimCollision::SetCollideBits(unsigned int /*_bits*/)
{
}

//////////////////////////////////////////////////
ignition::math::AxisAlignedBox OpensimCollision::BoundingBox() const
{
  WARN_FUNCTION_NOT_IMPLEMENTED
  ignition::math::AxisAlignedBox result;
  return result;
}

//////////////////////////////////////////////////
// void OpensimCollision::SetCollisionShape(OpenSim::ContactGeometry *_shape)
// {
//   this->collisionShape = _shape;
// }
//
// //////////////////////////////////////////////////
// OpenSim::ContactGeometry *OpensimCollision::GetCollisionShape() const
// {
//   return this->collisionShape;
// }
