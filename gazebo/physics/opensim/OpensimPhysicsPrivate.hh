/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#ifndef OPENSIMPHYSICSPRIVATE_HH
#define OPENSIMPHYSICSPRIVATE_HH

#include <cstdint>
#include <Simbody.h>
#include <OpenSim/OpenSim.h>
#include <boost/make_shared.hpp>

#include "gazebo/physics/opensim/OpensimLink.hh"
#include "gazebo/physics/opensim/OpensimJoint.hh"
#include "gazebo/physics/opensim/OpensimPhysics.hh"
#include "gazebo/physics/opensim/OpensimContactForceManager.hh"
#include "gazebo/physics/opensim/MobilityDiscreteForce.hh"

#include "gazebo/common/Time.hh"
#include "OpensimContactForceManager.hh"
#include "gazebo/common/Assert.hh"

namespace gazebo
{
  namespace physics
  {
    /* This object represents a mobilized body of simbody.
    * In gazebo it is associated with links, i.e. OpensimLinks own one or
    * more of these Mobs.
    * There can be many mobs per link which happens where a link is split
    * to remove loops in the system.
    * (See OpensimPhysics::AddModelToOpensimSystem)
    */
    struct Mob
    {
      /* The OpenSim api does not support copying of bodies in this
      * way, so we turn off assignment and copy ctor. */
      Mob(const Mob &) = delete;
      Mob& operator=(const Mob &) = delete;
      /* We want to construct this on the stack and later move it to its destination, though. */
      Mob();
      Mob& operator=(Mob &&) = default;
      Mob(Mob &&) = default;

      /* The Body instance is initialized with mass properties and Opensim internals
      * use it to get to the joint but the class does barely any work by itself. */
      boost::shared_ptr<OpenSim::Body> body;
      boost::shared_ptr<OpenSim::Joint> joint;
      boost::shared_ptr<OpenSim::WeldConstraint> slave_weld;
      double initial_state[6]; // At most 6 degrees of freedom. We store only the coordinates, not their velocities.
      OpensimJoint* gzJoint;
      OpensimLink* gzLink;
    };


//////////////////////////////////////////////////
    class OpensimPhysicsPrivate
    {
      public: OpensimPhysicsPrivate(WorldPtr _world, OpensimPhysics *_engine);

      public: void Load(sdf::ElementPtr _sdf);

      // Hopefully we never have to copy this thing.
      public: OpensimPhysicsPrivate(const OpensimPhysicsPrivate &) = delete;
      public: OpensimPhysicsPrivate& operator=(const OpensimPhysicsPrivate &) = delete;

#ifdef OPENSIM_USE_DEBUG_DRAWER
      public: gazebo::physics::DebugDrawer *debugDrawer;
#endif //OPENSIM_USE_DEBUG_DRAWER

      public: void AdjustBodyNameReferencesIn(SimTK::Xml::Element &_root, const OpensimModel &_model) const;
      public: void LoadMuscleDefinitionsAndDoSanityCheck(OpensimModel &_model);

      public: std::string CreateUniqueOsimNameFor(const Base *obj);
      public: std::string CreateUniquePostfix();

      public: std::string GetOsimNameFor(const OpensimSphereShape *obj)  const;
      public: std::string GetOsimNameFor(const OpensimPlaneShape *obj) const;
      public: std::string GetOsimNameFor(const OpensimMeshShape *obj) const;
      public: std::string GetOsimNameFor(const OpensimCollision *obj)  const;
      public: std::string GetOsimNameFor(const OpensimLink *obj) const;

      public: OpensimPhysics *engine;

      public: OpenSim::Model osimModel;

      /// @brief Obtain state realized to the given stage.
      ///
      /// Useful for lazy realization of the state in the case that some operation
      /// such as setting the velocity has invalidated the cache. Thus by calling
      /// getWorkingStateRealizedToStage we conveniently ensure that the cache
      /// is rebuild to the desired level. The function is essentially a wrapper
      /// around getMultibodySystem().realize(state, stage) and
      /// osimModel.getWorkingState().
      public: const SimTK::State& getWorkingStateRealizedToStage(SimTK::Stage stage) const;
      public: SimTK::State& updWorkingStateRealizedToStage(SimTK::Stage stage);

      /// @brief See CreateUniqueOsimNameFor
      private: std::uint64_t unique_name_counter;

      public: SimTK::Vec3 gravity_vector;

      /// \brief There are times the integrators internal MultibodySystem reference is invalidated. Then we have to recreate the integrator with an updated reference.
      public: void RegenerateIntegrator();

      public: double integratorAccuracy;
      public: double integratorMinStepSize;

      /// \brief The type of integrator:
      ///   SimTK::RungeKuttaMersonIntegrator(system)
      ///   SimTK::RungeKutta3Integrator(system)
      ///   SimTK::RungeKutta2Integrator(system)
      ///   SimTK::SemiExplicitEuler2Integrator(system)
      public: std::string integratorType;

      /// \brief An integrator picked from the settings in .sdf
      public: boost::shared_ptr<SimTK::Integrator>  integrator;

      public: OpenSim::Manager osimManager;

      public: OpensimContactForceManager contactForceManager;

      /// \brief Arbitrary discrete body forces and mobility (generalized) forces. 
      /// Useful for applying external forces or forces that are updated at discrete times due to the occurrence of events
      public: SimTK::Force::DiscreteForces discreteForces;

#ifdef TIME_UPDATE_PHYSICS // TODO: Want to wrap this in a nice class
      public: double meanUpdateTime;
      public: double sumSquareStdUpdateTime;
      public: int numUpdateCalls = { 0 };
      public: void addUpdateCall(const gazebo::common::Time &dt)
      {
        // See https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
        double dblDt = dt.Double();
        numUpdateCalls += 1;
        double lastMean = meanUpdateTime;
        meanUpdateTime += (1./numUpdateCalls) * (dblDt - meanUpdateTime);
        sumSquareStdUpdateTime += (dblDt - lastMean)*(dblDt - meanUpdateTime);
      }
      public: double getMeanUpdateTime() { return meanUpdateTime; }
      /// @brief Return the standard deviation of the calculated mean, reflecting the fluctuations of the mean around the true expectation.
      public: double getStdOfMeanUpdateTime()
      {

        // https://en.wikipedia.org/wiki/Law_of_large_numbers
        double var = sumSquareStdUpdateTime/numUpdateCalls;
        double std_of_mean = std::sqrt(var / numUpdateCalls);
        return std_of_mean;
      }
#endif
    };

//////////////////////////////////////////////////
    enum MobIndexStateIndicator {
      MOB_INDEX_UNINITIALIZED = -2,
      MOB_INDEX_STATIC_GROUND = -1
    };

//////////////////////////////////////////////////
    class OpensimLinkPrivate
    {
      public: OpensimLinkPrivate(OpensimLink* _link, OpensimPhysics *_engine)
        : engine(_engine), link(_link), master_index(MOB_INDEX_UNINITIALIZED) {}

      /// \brief Convert Gazebo Inertia to Opensim MassProperties
      /// Where Opensim MassProperties contains mass,
      /// center of mass location, and unit inertia about body origin.
      private: SimTK::MassProperties GetMassProperties() const;
      public: SimTK::MassProperties GetEffectiveMassProps(int _numFragments) const;

      public: inline bool isOsimRepresentationInitialized() const
      {
        return master_index != MOB_INDEX_UNINITIALIZED;
      }
      public: inline bool hasMobilizer() const
      {
        GZ_ASSERT(isOsimRepresentationInitialized(), "Must be initialized");
        return master_index >= 0;
      }
      public: bool isStatic() const
      {
        GZ_ASSERT(isOsimRepresentationInitialized(), "Must be initialized");
        GZ_ASSERT(link && (link->IsStatic() == (master_index == MOB_INDEX_STATIC_GROUND)), "Inconsistent staticness indicators");
        return master_index == MOB_INDEX_STATIC_GROUND;
      }
      /// @brief If has no mobilized body then return ground body, else return the body of this link. Trigger assert if not initialized.
      public: OpenSim::Body& getOsimBody();
      /// @brief Call getOsimBody and return the underlying MobilizedBody.
      public: SimTK::MobilizedBody& getSimTKBody();
      /// @brief Call getOsimBody and return the underlying MobilizedBody.
      public: const SimTK::MobilizedBody& getSimTKBody() const;
      /// @brief Reach through the SimTK physics system to get the transform of this link.
      public: SimTK::Transform getSimTkTransform(const SimTK::State &state) const;
      /// @brief Store current generalized coordinates of the mobilizer in Opensim's special storage for initial values.
      public: void SaveCoordinatesInDefaultValues(const SimTK::State &state);
      /// @brief Store coordinate default values in the Mob classes storage area.
      public: void SaveInitialCoordinates();
      /// @brief Fill default coordinates values with the stored values in the Mob class.
      public: void RestoreDefaultValuesToInitialCoordinates();
      /// @brief Between the slave and master mobs referenced already internally.
      public: void AddSlaveMasterWeldConstraintsTo(OpenSim::Model &_osimModel);
      /// @brief Returns the name of the Opensim Body instance, using its getName method.
      public: std::string GetOsimName() const;
      public: const SimTK::Force::DiscreteForces &getDiscreteForces() const;

      // Later there will potentially be more than one Mob in order to handle cutting of loops.
      public: OpensimPhysics *engine;
      public: OpensimLink* link;
      public: std::vector<Mob> mobs;
      public: int master_index;
      //public: bool osimRepresentationInitialized;
    };


//////////////////////////////////////////////////
    class OpensimJointPrivate
    {
      public: OpensimJointPrivate(OpensimJoint* _joint, OpensimPhysics *_engine);

      public: inline bool isOsimRepresentationInitialized() const
      {
        GZ_ASSERT((mob_index >= 0) == (outboardLink != nullptr), "If there is a link associated, the joint should also have a valid index into it's mob array");
        return mob_index != MOB_INDEX_UNINITIALIZED;
      }
      public: inline bool hasMobilizer() const
      {
        GZ_ASSERT(isOsimRepresentationInitialized(), "Must be initialized");
        return mob_index >= 0;
      }
      public: bool isStatic() const
      {
        GZ_ASSERT(isOsimRepresentationInitialized(), "Must be initialized");
        return mob_index == MOB_INDEX_STATIC_GROUND;
      }
      public: void SetOpenSimForceParameters(unsigned int _index);
      public: void InitializeJointForces();
      public: Mob& GetMob();
      public: const Mob& GetMob() const { return const_cast<OpensimJointPrivate*>(this)->GetMob(); }

      /// \brief keep a pointer to the opensim physics engine for convenience.
      /// Initialized with the proper reference in Ctor of derived classes.
      public: OpensimPhysics* engine;
      public: OpensimJoint* joint;

      /// \brief Force Opensim to break a loop by using a weld constraint.
      /// This flag is needed by OpensimPhysics::MultibodyGraphMaker, so kept
      /// public.
      public: bool mustBreakLoopHere;

      public: OpensimLink* outboardLink;
      public: int mob_index;

      public: OpenSim::MobilityDiscreteForce externalCoordinateForce[MAX_JOINT_AXIS];
      public: OpenSim::SpringGeneralizedForce springCoordinateForce[MAX_JOINT_AXIS];
      public: OpenSim::CoordinateLimitForce coordinateLimitForce[MAX_JOINT_AXIS];
      public: std::array<bool, MAX_JOINT_AXIS> isForceAddedToModel;
      public: bool osimRepresentationInitialized;
    };


//////////////////////////////////////////////////
    namespace opensim_internal
    {
      /// \brief Return the shape type as string.
      std::string ShapeTypeStr(unsigned int);

      /// \brief Convert ignition::math::Quaterniond to SimTK::Quaternion
      /// \param[in] _q Gazeb's math::Quaternion object
      /// \return Opensim's SimTK::Quaternion object
      SimTK::Quaternion QuadToQuad(const ignition::math::Quaterniond &_q);

      /// \brief Convert SimTK::Quaternion to ignition::math::Quaterniond
      /// \param[in] _q Opensim's SimTK::Quaternion object
      /// \return Gazeb's math::Quaternion object
      ignition::math::Quaterniond QuadToQuad(const SimTK::Quaternion &_q);

      /// \brief Convert ignition::math::Vector3d to SimTK::Vec3
      /// \param[in] _v Gazebo's ignition::math::Vector3d object
      /// \return Opensim's SimTK::Vec3 object
      SimTK::Vec3 Vector3ToVec3(const ignition::math::Vector3d &_v);

      /// \brief Convert SimTK::Vec3 to ignition::math::Vector3d
      /// \param[in] _v Opensim's SimTK::Vec3 object
      /// \return Gazebo's ignition::math::Vector3d object
      ignition::math::Vector3d Vec3ToVector3(const SimTK::Vec3 &_v);

      /// \brief Convert the given pose in x,y,z,thetax,thetay,thetaz format to
      /// a Opensim Transform. The rotation angles are interpreted as a
      /// body-fixed sequence, meaning we rotation about x, then about
      /// the new y, then about the now twice-rotated z.
      /// \param[in] _pose Gazeb's ignition::math::Pose3d object
      /// \return Opensim's SimTK::Transform object
      SimTK::Transform Pose2Transform(const ignition::math::Pose3d &_pose);

      /// \brief Convert a Opensim transform to a pose in x,y,z,
      /// thetax,thetay,thetaz format.
      /// \param[in] _xAB Opensim's SimTK::Transform object
      /// \return Gazeb's ignition::math::Pose3d object
      ignition::math::Pose3d Transform2Pose(const SimTK::Transform &_xAB);

      /// \brief If the given element contains a <pose> element, return it as a
      /// Transform. Otherwise return the identity Transform. If there
      /// is more than one <pose> element, only the first one is processed.
      SimTK::Transform GetPose(sdf::ElementPtr _element);

      /////////////////////////////////////////////////
      template<class T>
      static void remove(OpenSim::Set<T> &set, const T* item)
      {
          /* Yes, for each force we walk through the ForceSet array
          * and find its index. This is because OpenSim does not provide
          * an API to remove the force via the pointer!
          * Worse, every removal incurs the cost of shifiting all
          * following items forward. (It is named Set but really is
          * just a vector).
          * So total cost of removing all forces in the system O(N^3).
          * For a model with M forces it is O(N^2 * M)
          */
          int index = 0;
          for (; index <set.getSize(); ++index)
          {
            if (&set[index] == item)
              break;
          }
          if (index < set.getSize())
          {
            set.remove(index);
          }
      }
    } // namespace opensim_internal

    using namespace opensim_internal;
  }
}

#endif // OPENSIMPHYSICSPRIVATE_HH
