/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#ifndef _OPENSIM_INC_H_
#define _OPENSIM_INC_H_

#include <memory>
#include <thread>

// Silence warnings from unused function parameters.
#pragma GCC diagnostic ignored "-Wunused-parameter"
// Not returning anything when the function declaration says it should return something should be an error.
#pragma GCC diagnostic error "-Wreturn-type"

//#define OPENSIM_USE_DEBUG_DRAWER
//#define DEBUG_SPAM_IN_ACCESSORS

// Will use this to toggle printf debugging on/off during development
#define OPENSIM_PHYSICS_LOG_CALL()\
  gzdbg << __FUNCTION__ << " THREAD " << std::this_thread::get_id() << std::endl
#define OPENSIM_PHYSICS_LOG_CALL_ON(name)\
  gzdbg << __FUNCTION__ << " THREAD " << std::this_thread::get_id() << " on " << name << std::endl
#ifdef DEBUG_SPAM_IN_ACCESSORS
  #define OPENSIM_PHYSICS_LOG_ACCESSOR_ON(name)  OPENSIM_PHYSICS_LOG_CALL_ON(name)
#else
  #define OPENSIM_PHYSICS_LOG_ACCESSOR_ON(name)
#endif
#define OPENSIM_PHYSICS_DEBUG1(x)
#define OPENSIM_PHYSICS_DEBUG_UPDATE_LOOP(x)
#define OPENSIM_MUSCLE_DEBUG(x)
#define OPENSIM_MUSCLE_DEBUG_UPDATE_LOOP(x)
#define OPENSIM_CONTACT_DEBUG(x)
#define OPENSIM_CONTACT_DEBUG_UPDATE_LOOP(x)
#define OPENSIM_PHYSICS_DEBUG_ADD_MODEL(x) x

//#define TIME_UPDATE_PHYSICS

#define THROW_FUNCTION_NOT_IMPLEMENTED gzthrow(__FUNCTION__  << " not implemented!")

//#define WARN_FUNCTION_NOT_IMPLEMENTED { gzerr << __FUNCTION__ << " not implemented!" << std::endl; }
#define WARN_FUNCTION_NOT_IMPLEMENTED // No error spam here!

#ifndef ASSIGN_UNIQUE_OPENSIM_OBJECT_NAMES // may be defined in the cmake file
  #define ASSIGN_UNIQUE_OPENSIM_OBJECT_NAMES 1
#endif

namespace gazebo
{
  namespace physics
  {
    class OpensimLinkPrivate;
    class OpensimJointPrivate;
    class OpensimPhysicsPrivate;
    class OpensimSphereShape;
    class OpensimPlaneShape;
    class OpensimMeshShape;
    class OpensimLink;
    class OpensimJoint;
    class OpensimContactForceManager;
    class DebugDrawer;
  }
}

// Forward declaration. In preparation that we can
// eventually remove the inclusion of OpenSim headers.
namespace OpenSim
{
  class Model;
  class ContactHalfSpace;
  class Body;
  class Muscle;
  class ContactGeometry;
  class ContactMesh;
  class ContactSphere;
  class ContactPlane;
  class Actuator;
  class Actuator_;
  class PathActuator;
  class Force;
}

namespace SimTK
{
  class MultibodyGraphMaker;
  class ContactCliqueId;
  class State;
  class PolygonalMesh;
}

#endif
