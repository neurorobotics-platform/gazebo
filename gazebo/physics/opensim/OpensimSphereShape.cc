/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#include "gazebo/physics/opensim/OpensimCollision.hh"
#include "gazebo/physics/opensim/OpensimLink.hh"
#include "gazebo/physics/opensim/OpensimTypes.hh"
#include "gazebo/physics/opensim/OpensimSphereShape.hh"
#include "gazebo/physics/opensim/OpensimPhysics.hh"

#include "gazebo/common/Console.hh"
#include "gazebo/physics/World.hh"

#include "gazebo/physics/opensim/opensim_inc.h"
#include "OpensimPhysicsPrivate.hh"

using namespace gazebo;
using namespace physics;


OpensimSphereShape::~OpensimSphereShape()
{
}


void OpensimSphereShape::AddToBody(OpensimPhysicsPrivate* _engine_priv, OpenSim::Body &_body)
{
  OpensimCollisionPtr collision
    = boost::dynamic_pointer_cast<OpensimCollision>(this->collisionParent);

  ignition::math::Pose3d parent_pose = collision->RelativePose();
  SimTK::Vec3 sphere_pos(parent_pose.Pos().X(), parent_pose.Pos().Y(), parent_pose.Pos().Z());

  std::string osim_name = _engine_priv->CreateUniqueOsimNameFor(collision.get());
  this->osimGeometry = boost::make_shared<OpenSim::ContactSphere>(this->GetRadius(), sphere_pos, _body, osim_name);

  _engine_priv->osimModel.addContactGeometry(this->osimGeometry.get());

  gzdbg << "Created new OpenSim::ContactSphere: radius = " << this->GetRadius()
        << ", position = " << sphere_pos << ", name = " << this->osimGeometry->getName()
        << ", collision = " << collision->GetName()
        << ", to body = " << _body.getName() << std::endl;
}


void OpensimSphereShape::SetRadius(double _radius)
{
  if (_radius < 0)
  {
    gzerr << "Sphere shape does not support negative radius\n";
    return;
  }
  if (ignition::math::equal(_radius, 0.0))
  {
    // Warn user, but still create shape with very small value
    // otherwise later resize operations using setLocalScaling
    // will not be possible
    gzwarn << "Setting sphere shape's radius to zero \n";
    _radius = 1e-4;
  }
  SphereShape::SetRadius(_radius);
}
