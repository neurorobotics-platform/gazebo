/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#include "gazebo/physics/opensim/OpensimContactForceManager.hh"
#include "gazebo/physics/opensim/OpensimPhysicsPrivate.hh"
#include "gazebo/physics/opensim/OpensimCollision.hh"
#include "gazebo/physics/opensim/OpensimModel.hh"

#include "gazebo/common/Console.hh"
#include "gazebo/common/Exception.hh"

#include <unordered_set>
#include <boost/functional/hash.hpp>

using namespace gazebo;
using namespace physics;


OpensimContactForceManager::OpensimContactForceManager(OpensimPhysicsPrivate *_engine_priv)
    : engine_priv(_engine_priv)
{
}


OpensimContactForceManager::~OpensimContactForceManager()
{
}

// Because there are other types that are not supported, e.g ray.
inline static bool IsAdmissibleType(const Shape &s)
{
  return (s.HasType(Shape::SPHERE_SHAPE) ||
          s.HasType(Shape::PLANE_SHAPE) ||
          s.HasType(Shape::CYLINDER_SHAPE) ||
          s.HasType(Shape::MESH_SHAPE) ||
          s.HasType(Shape::BOX_SHAPE));
}


inline static bool AreAdjacent(const Link &a, const Link &b)
{
  // Slow. Makes copies of vectors and smart pointers.
  // If b is connected to a via a joint, it should be listed under the joints attached to a.
  // So there is no need to iterate over the joint lists of b.
  auto a_parent_joints = a.GetParentJoints();
  for (const auto &j : a_parent_joints)
  {
    if (j->GetParent().get() == &b) return true;
    if (j->GetChild().get() == &b) return true;
  }
  auto a_child_joints = a.GetChildJoints();
  for (const auto &j : a_child_joints)
  {
    if (j->GetParent().get() == &b) return true;
    if (j->GetChild().get() == &b) return true;
  }
  return false;
}


inline static bool IsAdmissibleCollisionPair(const Collision &col1, const Collision &col2)
{
  auto lnk1 = col1.GetLink();
  auto lnk2 = col2.GetLink();
  if (lnk1 && lnk2)
  {
    if (lnk1->IsStatic() && lnk2->IsStatic()) return false;
    if (lnk1->GetModel() == lnk2->GetModel())
    {
      if (!lnk1->GetModel()->GetSelfCollide()) return false;
      else return !AreAdjacent(*lnk1, *lnk2);
    }
  }
  return true;
}


namespace
{

/// @brief Add collision geometry to EF or HC force instance.
struct CollisionToForceAdderVisitor
{
  const OpensimCollision &collision_1;
  OpensimPhysicsPrivate* engine_priv;

  template <class ForceType>
  void operator()(ForceType &force)
  {
    std::string name_1 = engine_priv->GetOsimNameFor(&collision_1);
    const OpensimCollision::SurfaceParameters &sp_1 = collision_1.surfaceParameters;

    auto *params_1 =
        new typename ForceType::ContactParameters(
            sp_1.stiffness,
            sp_1.dissipation,
            sp_1.staticFriction,
            sp_1.dynamicFriction,
            sp_1.viscousFriction);
    params_1->addGeometry(name_1);
    params_1->setName(name_1); // Set name equal to the object it contains. For simple removal.

    OPENSIM_CONTACT_DEBUG(
        gzdbg << "To " << force.getName() << " added: " << name_1 << std::endl;
        gzdbg << "  stiffness = " << sp_1.stiffness << std::endl;
        gzdbg << "  dissipation = " << sp_1.dissipation << std::endl;
        gzdbg << "  staticFriction = " << sp_1.staticFriction << std::endl;
        gzdbg << "  dynamicFriction = " << sp_1.dynamicFriction << std::endl;
        gzdbg << "  viscousFriction = " << sp_1.viscousFriction << std::endl;)

    force.addContactParameters(params_1);
  }
};

} // anonymous namespace


OpensimContactForceManager::Force::Force(OpensimPhysicsPrivate *engine_priv, const OpensimCollision &collision_1, const OpensimCollision &collision_2)
  : collision_pair{&collision_1, &collision_2}
{
  engine_priv->osimModel.addForce(&this->ef);
  engine_priv->osimModel.addForce(&this->hc);
  OPENSIM_CONTACT_DEBUG(
  ef.setName("EF_"+collision_1.GetName()+"-"+collision_2.GetName());
  hc.setName("HC_"+collision_1.GetName()+"-"+collision_2.GetName());
  )
  CollisionToForceAdderVisitor{collision_1, engine_priv}(this->ef);
  CollisionToForceAdderVisitor{collision_1, engine_priv}(this->hc);
  CollisionToForceAdderVisitor{collision_2, engine_priv}(this->ef);
  CollisionToForceAdderVisitor{collision_2, engine_priv}(this->hc);
}


void OpensimContactForceManager::Force::RemoveForces(OpensimPhysicsPrivate *engine_priv)
{
  remove<OpenSim::Force>(engine_priv->osimModel.updForceSet(), &this->ef);
  remove<OpenSim::Force>(engine_priv->osimModel.updForceSet(), &this->hc);
}


void OpensimContactForceManager::Force::InsertContactDataIn(OpensimContactForceManager::ContactData &contact_data)
{
  {
    const auto &cd = hc.getHuntCrossleyForce()->getContactData();
    std::copy(cd.begin(), cd.end(), std::back_inserter(contact_data));
  }
  {
    const auto &cd = ef.getElasticFoundationForce()->getContactData();
    std::copy(cd.begin(), cd.end(), std::back_inserter(contact_data));
  }
}


void OpensimContactForceManager::AddToContactForceSet(const Collision_V &new_collisions)
{
  OPENSIM_PHYSICS_DEBUG1(gzdbg << __FUNCTION__ << std::endl;)

  for (std::size_t i = 0; i < new_collisions.size(); ++i)
  {
    const OpensimCollision &collision_1 = static_cast<const OpensimCollision&>(*new_collisions[i]);
    Shape *shape_1 = collision_1.GetShape().get();

    if (!IsAdmissibleType(*shape_1))
    {
      OPENSIM_CONTACT_DEBUG(gzdbg << "Shape of " << collision_1.GetScopedName() << " (" << ShapeTypeStr(shape_1->GetType()) << ") unsupported -> Skipped" << std::endl;);
      continue;
    }

    for (std::size_t k = i+1; k < new_collisions.size(); ++k)
    {
      auto &collision_2 = static_cast<OpensimCollision&>(*new_collisions[k]);
      if (IsAdmissibleCollisionPair(collision_1, collision_2))
      {
        boost::shared_ptr<Force> force_holder = boost::make_shared<Force>(
          this->engine_priv,
          collision_1,
          collision_2);
        forces.push_back(std::move(force_holder));
      }
    }

    for (std::size_t k = 0; k < this->all_collisions.size(); ++k)
    {
      auto &collision_2 = static_cast<OpensimCollision&>(*all_collisions[k]);
      if (IsAdmissibleCollisionPair(collision_1, collision_2))
      {
        boost::shared_ptr<Force> force_holder = boost::make_shared<Force>(
          this->engine_priv,
          collision_1,
          collision_2);
        forces.push_back(std::move(force_holder));
      }
    }

    all_collisions.push_back(new_collisions[i]);
  }
}


void OpensimContactForceManager::RemoveFromContactForceSet(const Collision_V &collisions_to_delete)
{
  OPENSIM_PHYSICS_DEBUG1(gzdbg << __FUNCTION__ << std::endl;)

  std::unordered_set<const Collision*> set_to_delete;
  set_to_delete.reserve(collisions_to_delete.size());
  std::transform(
    collisions_to_delete.begin(),
    collisions_to_delete.end(),
    std::inserter(set_to_delete, set_to_delete.end()),
    [](const CollisionPtr &p) { return p.get(); });

  {
    auto check_remove_and_maybe_cleanup = [this, &set_to_delete](boost::shared_ptr<Force> &f) -> bool
    {
      if (set_to_delete.find(f->collision_pair[0]) != set_to_delete.end() ||
          set_to_delete.find(f->collision_pair[1]) != set_to_delete.end())
      {
        f->RemoveForces(this->engine_priv); // Cleanup that I don't want to do in the destructor.
        return true;
      }
      else
        return false;
    };
    auto new_end = std::remove_if(forces.begin(), forces.end(), check_remove_and_maybe_cleanup);
    forces.erase(new_end, forces.end());
  }

  {
    auto new_end = std::remove_if(all_collisions.begin(), all_collisions.end(),
      [this, &set_to_delete](const CollisionPtr &p) -> bool {
        return set_to_delete.find(p.get()) != set_to_delete.end();
      });
    all_collisions.erase(new_end, all_collisions.end());
  }
}


void OpensimContactForceManager::UpdateContactData()
{
  this->contact_data.clear();
  for (std::size_t i=0; i<forces.size(); ++i)
  {
    forces[i]->InsertContactDataIn(this->contact_data);
  }
}
