
/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#ifndef _GAZEBO_OPENSIM_TYPES_HH_
#define _GAZEBO_OPENSIM_TYPES_HH_

#include <boost/shared_ptr.hpp>

/* boost lib has a make_unique variant that works with pure c++11. However it
 * throws a static_assert because SimTK::Integrator has no virtual destructor.
 * Normally this is a nice thing to do but in this special case the Integrator
 * is fine without the virtual destructor. Hence I cannot use this.
 */
//#include <boost/move/unique_ptr.hpp>
//#include <boost/move/make_unique.hpp>

#include "gazebo/util/system.hh"
#include <vector>
/// \file
/// \ingroup gazebo_physics
/// \ingroup gazebo_physics_opensim
/// \brief Opensim wrapper forward declarations and typedefs
namespace gazebo
{
  namespace physics
  {
    /// \addtogroup gazebo_physics_opensim
    /// \{

    class OpensimCollision;
    class OpensimLink;
    class OpensimJoint;
    class OpensimModel;
    class OpensimPhysics;
    class OpensimRayShape;
    class OpensimPathActuator;

    /// \def OpensimPhysicsPtr
    /// \brief Boost shared point to OpensimPhysics
    typedef boost::shared_ptr<OpensimPhysics> OpensimPhysicsPtr;

    /// \def OpensimCollisionPtr
    /// \brief Boost shared point to OpensimCollision
    typedef boost::shared_ptr<OpensimCollision> OpensimCollisionPtr;

    /// \def OpensimLinkPtr
    /// \brief Boost shared point to OpensimLink
    typedef boost::shared_ptr<OpensimLink> OpensimLinkPtr;

    /// \def OpensimJointPtr
    /// \brief Boost shared pointer to OpensimJoint
    typedef boost::shared_ptr<OpensimJoint> OpensimJointPtr;

    /// \def OpensimModelPtr
    /// \brief Boost shared point to OpensimModel
    typedef boost::shared_ptr<OpensimModel> OpensimModelPtr;

    /// \def OpensimRayShapePtr
    /// \brief Boost shared point to OpensimRayShape
    typedef boost::shared_ptr<OpensimRayShape> OpensimRayShapePtr;
    /// \}

    /// \def OpensimMusclePtr
    /// \brief Boost shared pointer to OpensimMuscle
    typedef boost::shared_ptr<OpensimPathActuator> OpensimMusclePtr;

    /// \def Muscle_V
    /// \brief vector of shared pointers to Muscles
    typedef std::vector<OpensimMusclePtr> Muscle_V;
  }
}

#endif