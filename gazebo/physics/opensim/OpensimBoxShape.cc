#include "gazebo/physics/opensim/OpensimCollision.hh"
#include "gazebo/physics/opensim/OpensimLink.hh"
#include "gazebo/physics/opensim/OpensimTypes.hh"
#include "gazebo/physics/opensim/OpensimPhysics.hh"
#include "gazebo/physics/opensim/OpensimBoxShape.hh"
#include "gazebo/physics/opensim/OpensimMeshShape.hh"
#include "gazebo/common/MeshManager.hh"
#include "gazebo/common/Console.hh"
#include "gazebo/physics/World.hh"

#include "gazebo/physics/opensim/opensim_inc.h"
#include "OpensimPhysicsPrivate.hh"

#include <ignition/math/Vector3.hh>


using namespace gazebo;
using namespace physics;

void OpensimBoxShape::SetSize(const ignition::math::Vector3d &_size)
{
  // we will have to finish the implementation with making the new size known to simbody
  if (_size.X() < 0 || _size.Y() < 0 || _size.Z() < 0)
  {
    gzerr << "Box shape does not support negative size\n";
    return;
  }
  ignition::math::Vector3d size = _size;
  if (ignition::math::equal(size.X(), 0.0))
  {
    // Warn user, but still create shape with very small value
    // otherwise later resize operations using setLocalScaling
    // will not be possible
    gzwarn << "Setting box shape's x to zero \n";
    size.X(1e-4);
  }
  if (ignition::math::equal(size.Y(), 0.0))
  {
    gzwarn << "Setting box shape's y to zero \n";
    size.Y(1e-4);
  }
  if (ignition::math::equal(size.Z(), 0.0))
  {
    gzwarn << "Setting box shape's z to zero \n";
    size.Z(1e-4);
  }

  BoxShape::SetSize(size);
}


//////////////////////////////////////////////////
void OpensimBoxShape::Init()
{
  OPENSIM_PHYSICS_LOG_CALL_ON(this->GetScopedName());
  BoxShape::Init();
}

//////////////////////////////////////////////////
void OpensimBoxShape::AddToBody(OpensimPhysicsPrivate *_engine_priv, OpenSim::Body &_body)
{
  OpensimCollisionPtr collision = boost::dynamic_pointer_cast<OpensimCollision>(this->collisionParent);

  SimTK::Transform X_LC =
      Pose2Transform(
          collision->RelativePose());

  // convert to position/euler representation
  SimTK::Vec3 pos = X_LC.p();
  SimTK::Vec3 euler = X_LC.R().convertRotationToBodyFixedXYZ();

  const char* MESH_NAME = "OPENSIM_BOX_MESH";
  gazebo::common::MeshManager::Instance()->CreateCollisionBox(
    MESH_NAME, ignition::math::Vector3d::One, 2,2,2); // Returns cached mesh by name if created before.
  const common::Mesh* mesh = gazebo::common::MeshManager::Instance()->GetMesh(MESH_NAME);

  // The sdf format does not specify the scale tag for boxes. It is therefore
  // sufficient to consider GetSize() for correct sizing.
  SimTK::PolygonalMesh simtk_mesh;
  ConvertToSimTK(simtk_mesh, *mesh, this->Size());

  std::string osim_name = _engine_priv->CreateUniqueOsimNameFor(collision.get());
  this->osimGeometry = boost::make_shared<OpenSim::ContactMesh>(
    simtk_mesh, pos, euler, _body, osim_name);

  _engine_priv->osimModel.addContactGeometry(this->osimGeometry.get());

  OPENSIM_PHYSICS_DEBUG1(gzdbg << "Created new OpenSim ContactMesh for "
                              << collision->GetName() << " (" << osim_name << ") "
                              << ", position = " << pos
                              << ", orientation = " << euler
                              << std::endl;)
}
