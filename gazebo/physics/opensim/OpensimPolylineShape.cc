/*
 * Copyright (C) 2014-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#include "gazebo/common/Mesh.hh"
#include "gazebo/common/Console.hh"
#include "gazebo/physics/opensim/OpensimCollision.hh"
#include "gazebo/physics/opensim/OpensimPhysics.hh"
#include "gazebo/physics/opensim/OpensimPolylineShape.hh"

using namespace gazebo;
using namespace physics;

//////////////////////////////////////////////////
OpensimPolylineShape::OpensimPolylineShape(CollisionPtr _parent)
    : PolylineShape(_parent), opensimMesh(NULL)
{
  // this->opensimMesh = new OpensimMesh(this);
}

//////////////////////////////////////////////////
OpensimPolylineShape::~OpensimPolylineShape()
{
  // delete this->opensimMesh;
}

//////////////////////////////////////////////////
void OpensimPolylineShape::Load(sdf::ElementPtr _sdf)
{
  PolylineShape::Load(_sdf);
}

//////////////////////////////////////////////////
void OpensimPolylineShape::Init()
{
  PolylineShape::Init();
  if (!this->mesh)
  {
    gzerr << "Unable to create polyline in Opensim. Mesh pointer is null.\n";
    return;
  }

  gzerr << "Polyline shapes are not supported in Opensim\n";

  // Uncomment these lines when opensim supports mesh shapes.
  // this->opensimMesh->Init(this->mesh,
  //     boost::static_pointer_cast<OpensimCollision>(this->collisionParent),
  //     ignition::math::Vector3d(1, 1, 1));
}
