/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#include "gazebo/common/Assert.hh"
#include "gazebo/common/Console.hh"
#include "gazebo/common/Exception.hh"

#include "gazebo/physics/opensim/OpensimTypes.hh"
#include "gazebo/physics/opensim/OpensimLink.hh"
#include "gazebo/physics/opensim/OpensimPhysics.hh"
#include "gazebo/physics/opensim/OpensimHinge2Joint.hh"

using namespace gazebo;
using namespace physics;

//////////////////////////////////////////////////
OpensimHinge2Joint::OpensimHinge2Joint(BasePtr _parent, OpensimPhysics &_engine)
    : Hinge2Joint<OpensimJoint>(_parent)
{
}

//////////////////////////////////////////////////
OpensimHinge2Joint::~OpensimHinge2Joint()
{
}

//////////////////////////////////////////////////
void OpensimHinge2Joint::Load(sdf::ElementPtr _sdf)
{
  Hinge2Joint<OpensimJoint>::Load(_sdf);
}

//////////////////////////////////////////////////
ignition::math::Vector3d OpensimHinge2Joint::Anchor(unsigned int /*index*/) const
{
  return this->anchorPos;
}

//////////////////////////////////////////////////
// ignition::math::Vector3d OpensimHinge2Joint::GetAxis(unsigned int /*index*/) const
// {
//   gzerr << "Not implemented";
//   return ignition::math::Vector3d();
// }

//////////////////////////////////////////////////
double OpensimHinge2Joint::GetVelocity(unsigned int /*_index*/) const
{
  gzerr << "Not implemented";
  return 0;
}

//////////////////////////////////////////////////
void OpensimHinge2Joint::SetVelocity(unsigned int /*_index*/,
                                     double /*_angle*/)
{
  gzerr << "Not implemented";
}

//////////////////////////////////////////////////
void OpensimHinge2Joint::SetAxis(unsigned int /*_index*/,
                                 const ignition::math::Vector3d & /*_axis*/)
{
  // Opensim seems to handle setAxis improperly. It readjust all the pivot
  // points
}

//////////////////////////////////////////////////
void OpensimHinge2Joint::SetForceImpl(
    unsigned int /*_index*/, double /*_torque*/)
{
  gzerr << "Not implemented";
}

//////////////////////////////////////////////////
ignition::math::Vector3d OpensimHinge2Joint::GlobalAxis(unsigned int /*_index*/) const
{
  gzerr << "OpensimHinge2Joint::GlobalAxis not implemented\n";
  return ignition::math::Vector3d();
}

//////////////////////////////////////////////////
double OpensimHinge2Joint::PositionImpl(const unsigned int /*_index*/) const
{
  gzerr << "OpensimHinge2Joint::PositionImpl not implemented\n";
  return 0.0;
}
