/*
 * Copyright (C) 2015-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#ifndef _GAZEBO_OPENSIM_FIXEDJOINT_HH_
#define _GAZEBO_OPENSIM_FIXEDJOINT_HH_

#include <vector>

#include "gazebo/physics/FixedJoint.hh"
#include "gazebo/physics/opensim/OpensimJoint.hh"
#include "gazebo/physics/opensim/OpensimPhysics.hh"
#include "gazebo/util/system.hh"

namespace gazebo
{
namespace physics
{
/// \ingroup gazebo_physics
/// \addtogroup gazebo_physics_opensim Opensim Physics
/// \{

/// \brief A fixed joint rigidly connecting two bodies
class GZ_PHYSICS_VISIBLE OpensimFixedJoint : public FixedJoint<OpensimJoint>
{
  /// \brief Constructor
  /// \param[in] world pointer to the opensim world
  /// \param[in] _parent pointer to the parent Model
public:
  OpensimFixedJoint(BasePtr _parent, OpensimPhysics &_engine);

  /// \brief Destructor
public:
  virtual ~OpensimFixedJoint() override;

  // Documentation inherited.
protected:
  virtual void Load(sdf::ElementPtr _sdf) override;

  //       // Documentation inherited.
  //       public: virtual double GetVelocity(unsigned int _index) const  override;
  //
  //       // Documentation inherited.
  //       public: virtual void SetVelocity(unsigned int _index, double _angle) override;
  //
  //       // Documentation inherited.
  //       public: virtual void SetForceImpl(unsigned int _index, double _torque) override;
  //
  //       // Documentation inherited.
  //       public: virtual ignition::math::Vector3d GetGlobalAxis(unsigned int _index) const override;
  //
  //       // Documentation inherited.
  //       public: virtual ignition::math::Angle GetAngleImpl(unsigned int _index) const override;

protected:
  virtual bool CheckValidAxisAndDebugOut(unsigned int _axis, const char *_where) const override;
};
/// \}
}
}
#endif
