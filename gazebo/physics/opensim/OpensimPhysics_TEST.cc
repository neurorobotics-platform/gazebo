/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#include <gtest/gtest.h>
#include <string>

#include "gazebo/physics/physics.hh"
#include "gazebo/physics/PhysicsEngine.hh"
#include "gazebo/physics/opensim/OpensimTypes.hh"
#include "gazebo/physics/opensim/OpensimPhysics.hh"
#include "gazebo/physics/opensim/OpensimCollision.hh"


#include "gazebo/common/Mesh.hh"
#include "gazebo/common/MeshManager.hh"
#include "gazebo/common/Material.hh"
#include "gazebo/common/ColladaLoader.hh"
#include "gazebo/common/OBJExporter.hh"

#include "gazebo/test/ServerFixture.hh"
#include "test/util.hh"
#include "gazebo/physics/PhysicsTypes.hh"
#include "gazebo/physics/Model.hh"

#include "gazebo/msgs/msgs.hh"

using namespace gazebo;
using namespace physics;

class OpensimPhysics_TEST : public ServerFixture
{
protected:
  OpensimPhysics_TEST() : ServerFixture()
  {
    gazebo::common::Console::SetColored(false); // Because it mangles the output in jenkins.
  }
};



/////////////////////////////////////////////////
TEST_F(OpensimPhysics_TEST, ObjExporter)
{
  common::ColladaLoader loader;
  common::Mesh *mesh = loader.Load(
  std::string(PROJECT_SOURCE_PATH) + "/test/data/cube_test.dae");
  ASSERT_TRUE(mesh != NULL);
}

#if 0 // Does not work on Jenkins.
/////////////////////////////////////////////////
TEST_F(OpensimPhysics_TEST, MuscleLoadingSanityCheck)
{
  boost::filesystem::path path = PROJECT_SOURCE_PATH;
  path /= "test";
  path /= "models";
  path /= "opensim";
  gazebo::common::SystemPaths::Instance()->AddModelPaths(path.string());

  Load(std::string(PROJECT_SOURCE_PATH) + "/test/worlds/opensim/muscle_wrap_test.world", true, "opensim");
  physics::WorldPtr world = physics::get_world("default");
  ASSERT_TRUE(world != NULL);
  world->Step(1);
}
#endif

#if 0
/////////////////////////////////////////////////
TEST_F(OpensimPhysics_TEST, ReadContactParametersFromSdf)
{
  boost::filesystem::path path = PROJECT_SOURCE_PATH;
  path /= "test";
  path /= "models";
  path /= "opensim";
  gazebo::common::SystemPaths::Instance()->AddModelPaths(path.string());

  Load(std::string(PROJECT_SOURCE_PATH) + "/test/worlds/opensim/bouncing_spheres_contact_parameter_variation.world", true, "opensim");

  physics::WorldPtr world = physics::get_world("the_world");
  ASSERT_TRUE(world != NULL);

  physics::ModelPtr model1 = world->GetModel("model_default");
  physics::ModelPtr model2 = world->GetModel("model_friction");
  physics::ModelPtr model3 = world->GetModel("model_poisson_and_elastic_modulus");
  physics::ModelPtr model4 = world->GetModel("model_stiff7_diss-2");

  physics::LinkPtr link1 = model1->GetLink("the_body");
  physics::LinkPtr link2 = model2->GetLink("the_body");
  physics::LinkPtr link3 = model3->GetLink("the_body");
  physics::LinkPtr link4 = model4->GetLink("the_body");

  static constexpr unsigned int ZERO = 0;
  const physics::OpensimCollisionPtr coll1 =
    boost::dynamic_pointer_cast<physics::OpensimCollision>(
      link1->GetCollision(ZERO));
  const physics::OpensimCollisionPtr coll2 =
    boost::dynamic_pointer_cast<physics::OpensimCollision>(
      link2->GetCollision(ZERO));
  const physics::OpensimCollisionPtr coll3 =
    boost::dynamic_pointer_cast<physics::OpensimCollision>(
      link3->GetCollision(ZERO));
  const physics::OpensimCollisionPtr coll4 =
    boost::dynamic_pointer_cast<physics::OpensimCollision>(
      link4->GetCollision(ZERO));

  // Actual default overridden by global settings in world physics section
  static constexpr double REL_ERR = 0.01;
  EXPECT_NEAR(coll1->surfaceParameters.stiffness, 1.23e5, 1.23e5 * REL_ERR);
  EXPECT_NEAR(coll1->surfaceParameters.dissipation, 0.00123, 0.00123 * REL_ERR);
  EXPECT_NEAR(coll1->surfaceParameters.transitionVelocity, 0.666, 0.666 * REL_ERR);
  EXPECT_NEAR(coll1->surfaceParameters.staticFriction, 0.123, 0.123 * REL_ERR);
  EXPECT_NEAR(coll1->surfaceParameters.dynamicFriction, 0.456, 0.456 * REL_ERR);
  EXPECT_NEAR(coll1->surfaceParameters.viscousFriction, 0.789, 0.789 * REL_ERR);

  // Collision local setting of friction parameters
  EXPECT_NEAR(coll2->surfaceParameters.transitionVelocity, 0.1337, 0.1337 * REL_ERR);
  EXPECT_NEAR(coll2->surfaceParameters.staticFriction, 0.098, 0.098 * REL_ERR);
  EXPECT_NEAR(coll2->surfaceParameters.dynamicFriction, 0.765, 0.765 * REL_ERR);
  EXPECT_NEAR(coll2->surfaceParameters.viscousFriction, 0.432, 0.432 * REL_ERR);

  // Collision local setting of bouncyness parameters
  EXPECT_NEAR(coll4->surfaceParameters.stiffness, 1.e7, 1.e7 * REL_ERR);
  EXPECT_NEAR(coll4->surfaceParameters.dissipation, 0.01, 0.01 * REL_ERR);

  // Collision local setting of stiffness using elastic_modulus and poissons_ratio
  double elastic_modulus = 1.e5;
  double poissons_ratio = 0.5;
  double tmp = (1.0 - poissons_ratio * poissons_ratio);
  double stiffness = elastic_modulus / tmp;
  EXPECT_NEAR(coll3->surfaceParameters.stiffness, stiffness, stiffness * REL_ERR);
}
#endif

/////////////////////////////////////////////////
/// Main
int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
