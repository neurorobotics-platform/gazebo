/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#ifndef _OPENSIM_UNIVERSAL_JOINT_HH_
#define _OPENSIM_UNIVERSAL_JOINT_HH_

#include "gazebo/physics/UniversalJoint.hh"
#include "gazebo/physics/opensim/OpensimJoint.hh"
#include "gazebo/physics/opensim/OpensimPhysics.hh"
#include "gazebo/util/system.hh"

namespace gazebo
{
namespace physics
{
/// \ingroup gazebo_physics
/// \addtogroup gazebo_physics_opensim Opensim Physics
/// \{

/// \brief A opensim universal joint class
class GZ_PHYSICS_VISIBLE OpensimUniversalJoint
    : public UniversalJoint<OpensimJoint>
{
  /// \brief Constructor
  /// \param[in] _world Pointer to the Opensim world.
  /// \param[in] _parent Parent of the screw joint.
public:
  OpensimUniversalJoint(BasePtr _parent, OpensimPhysics &_engine);

  /// \brief Destuctor
public:
  virtual ~OpensimUniversalJoint() override;

  // Documentation inherited.
public:
  virtual void Load(sdf::ElementPtr _sdf) override;

  // Documentation inherited.
public:
  virtual ignition::math::Vector3d Anchor(unsigned int _index) const override;

  // Documentation inherited.
public:
  virtual void SetAxis(unsigned int _index,
                       const ignition::math::Vector3d &_axis) override;

  // Documentation inherited.
  //public: virtual ignition::math::Vector3d GetAxis(unsigned int _index) const;

  // Documentation inherited.
public:
  virtual void SetVelocity(unsigned int _index, double _rate) override;

  // Documentation inherited.
public:
  virtual double GetVelocity(unsigned int _index) const override;

  // Documentation inherited.
public:
  virtual ignition::math::Vector3d GlobalAxis(unsigned int _index) const override;

  // Documentation inherited.
protected:
  virtual double PositionImpl(unsigned int _index) const override;

  // Documentation inherited.
protected:
  virtual void SetForceImpl(unsigned int _index, double _torque) override;
};
/// \}
}
}
#endif
