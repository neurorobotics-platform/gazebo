/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#ifndef _OPENSIM_MODEL_HH_
#define _OPENSIM_MODEL_HH_

#include "gazebo/physics/Model.hh"
#include "gazebo/physics/opensim/OpensimMuscle.hh"
#include "gazebo/util/system.hh"
#include "gazebo/physics/opensim/OpensimTypes.hh"
#include "gazebo/physics/opensim/opensim_inc.h"
#include <vector>

namespace gazebo
{
  namespace physics
  {
    /// \addtogroup gazebo_physics_opensim
    /// \{

    /// \class OpensimModel OpensimModel.hh physics/physics.hh
    /// \brief A model is a collection of links, joints, and plugins.
    class GZ_PHYSICS_VISIBLE OpensimModel : public Model
    {
      /// \brief Constructor.
      /// \param[in] _parent Parent object.
      public: OpensimModel(BasePtr _parent, OpensimPhysics *_engine);

      /// \brief Destructor.
      public: virtual ~OpensimModel() override;

      // Documentation inherited
      public: virtual void Load(sdf::ElementPtr _sdf) override;

      public: const Muscle_V& GetMuscles() const;

      // Documentation inherited
      public: virtual void Init() override;

      public: virtual void Fini() override;

      public: virtual void Reset() override;

      public: bool IsRootModel() const;

      private: friend class OpensimPhysics;
      private: friend class OpensimPhysicsPrivate;

      /// \brief Hold on to the physics engine that created this. It should be alive during the lifetime of this.
      private: OpensimPhysics *engine;

      /// \brief Hold on to all forces that need to be delete when the model is removed.
      private: std::vector<boost::shared_ptr<OpenSim::Force>> owned_osim_forces;

      /// \brief The file to load the muscle definitions from.
      private: std::string muscle_definition_filename;

      private: Muscle_V muscles;

      private: friend class RemovingModels;
      private: friend class OpensimPhysicsPrivate;
    };
    /// \}
  }
}
#endif
