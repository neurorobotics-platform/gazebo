/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#include "gazebo/physics/World.hh"
#include "gazebo/physics/opensim/OpensimLink.hh"
#include "gazebo/physics/opensim/OpensimPhysics.hh"
#include "gazebo/physics/opensim/OpensimTypes.hh"
#include "gazebo/physics/opensim/OpensimCollision.hh"
#include "gazebo/physics/opensim/OpensimRayShape.hh"

using namespace gazebo;
using namespace physics;

//////////////////////////////////////////////////
OpensimRayShape::OpensimRayShape(PhysicsEnginePtr _physicsEngine)
    : RayShape(_physicsEngine)
{
  this->SetName("Opensim Ray Shape");

  this->physicsEngine =
      boost::static_pointer_cast<OpensimPhysics>(_physicsEngine);
}

//////////////////////////////////////////////////
OpensimRayShape::OpensimRayShape(CollisionPtr _parent)
    : RayShape(_parent)
{
  this->SetName("Opensim Ray Shape");
  this->physicsEngine = boost::static_pointer_cast<OpensimPhysics>(
      this->collisionParent->GetWorld()->Physics());
}

//////////////////////////////////////////////////
OpensimRayShape::~OpensimRayShape()
{
}

//////////////////////////////////////////////////
void OpensimRayShape::Update()
{
}

//////////////////////////////////////////////////
void OpensimRayShape::GetIntersection(double &_dist, std::string &_entity)
{
  _dist = 0;
  _entity = "";

  if (this->physicsEngine)
  {
  }
}

//////////////////////////////////////////////////
void OpensimRayShape::SetPoints(const ignition::math::Vector3d &_posStart,
                                const ignition::math::Vector3d &_posEnd)
{
  this->globalStartPos = _posStart;
  this->globalEndPos = _posEnd;
}
