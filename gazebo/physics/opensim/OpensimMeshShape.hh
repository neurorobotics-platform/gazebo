/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#ifndef _OPENSIM_MESHSHAPE_HH_
#define _OPENSIM_MESHSHAPE_HH_

#include "gazebo/physics/MeshShape.hh"
#include "gazebo/util/system.hh"
#include "gazebo/physics/opensim/opensim_inc.h"
#include "gazebo/physics/opensim/OpensimTypes.hh"

namespace gazebo
{
  namespace physics
  {
    namespace opensim_internal
    {
      std::string MakeTempFilePath(const Shape *shape, const Collision* collision = nullptr);
      void ConvertToSimTK(SimTK::PolygonalMesh &simtk_mesh, const common::Mesh &mesh, const ignition::math::Vector3d &scale_factor);
    }

    class OpensimMesh;

    /// \ingroup gazebo_physics
    /// \addtogroup gazebo_physics_opensim Opensim Physics
    /// \{

    /// \brief Triangle mesh collision
    class GZ_PHYSICS_VISIBLE OpensimMeshShape : public MeshShape
    {
      /// \brief Constructor
      public: OpensimMeshShape(CollisionPtr _parent);

      /// \brief Destructor
      public: virtual ~OpensimMeshShape() override;

      // Documentation inherited
      public: virtual void Load(sdf::ElementPtr _sdf) override;

      /// \brief Add the collision shape.
      ///
      /// Called by the physics engine so that shapes can add their
      /// add geometry to the OpenSim::Model contained in _engine.
      private: void AddToBody(OpensimPhysicsPrivate* _engine, OpenSim::Body &_body);

      /// Documentation inherited
      protected: virtual void Init() override;

      /// \brief Holds opensim geometry
      private: boost::shared_ptr<OpenSim::ContactMesh> osimGeometry;

      // For easy access from the physics engine
      private:
        friend class OpensimPhysics;
        friend class OpensimPhysicsPrivate;
    };
    /// \}
  }
}
#endif

