/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#ifndef _CONTACTVISUAL_PRIVATE_HH_
#define _CONTACTVISUAL_PRIVATE_HH_

#include <string>
#include <vector>

#include "gazebo/msgs/msgs.hh"
#include "gazebo/transport/TransportTypes.hh"
#include "gazebo/rendering/RenderTypes.hh"
#include "gazebo/rendering/VisualPrivate.hh"

namespace gazebo
{
  namespace rendering
  {
    namespace muscle_visual_internal
    {
    // NOTE: using std::shared_ptr everywhere.
    // Also note: shared_from_this(), works only
    // on a previously shared object.
    // See http://en.cppreference.com/w/cpp/memory/enable_shared_from_this/shared_from_this

    /// \brief Single muscle (fiber) visual
    class FiberVisual : public Visual
    {
      public: FiberVisual(const std::string &_name, VisualPtr _parent,
                          bool _useRTShader);

      public: ~FiberVisual();

      /// \brief Manual cleanup of contained visuals.
      public: void Fini() override;

      /// \brief Create visuals as needed and adjust parameters.
      public: void UpdateFromMsg(const msgs::OpenSimMuscle &msg, double radius);
    };


    /// \brief Private data for the muscle fiber
    class FiberVisualPrivate : public VisualPrivate
    {
      /// \brief Just the ctor.
      public: FiberVisualPrivate();

      /// \brief Shows locations of attachment points
      public: VisualPtr attachmentPointVisual[2];

      /// \brief Visualizes muscle path segments
      public: std::vector<VisualPtr> pathSegmentVisuals;

      /// \brief The scalar from which the colorization was computed last.
      public: float last_colorization_basis_value;

      /// \brief Need to keep track of that in order to be able to trigger color updates.
      public: int last_muscle_kind;
    };


    /// \brief Private data for the global muscle visual
    class OpenSimMuscleVisualPrivate : public VisualPrivate
    {
      /// \brief Node for communication.
      public: transport::NodePtr node;

      /// \brief Subscription to the contact data.
      public: transport::SubscriberPtr openSimSub;

      /// \brief The current contact message.
      public: boost::shared_ptr<msgs::OpenSimMuscles const> openSimMsg;

      /// \brief All the event connections.
      public: std::vector<event::ConnectionPtr> connections;

      /// \brief All muscles (fibers)
      public: std::vector<std::shared_ptr<FiberVisual>> muscle_visuals;

      /// \brief Cached. Computed from muscle message.
      public: double fiber_radius;

      /// \brief So we can recompute the radius if the number of muscles changes.
      public: int last_muscle_count;

      /// \brief Mutex to protect the message.
      public: boost::mutex mutex;

      /// \brief True if we have received a message.
      public: bool receivedMsg;

      /// \brief True if this visualization is enabled.
      public: bool enabled;

      /// \brief Name of the topic contact information is published on
      public: std::string topicName;
    };

    } // namespace
  } // namespace
} // namespace
#endif
