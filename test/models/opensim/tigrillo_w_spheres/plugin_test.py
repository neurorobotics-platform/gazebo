#! /usr/bin/env python

import rospy
import math
import time

from std_msgs.msg import Float32MultiArray, MultiArrayLayout

# Create node
rospy.init_node('test_w_spheres_tigrillo', anonymous=True)
pub = rospy.Publisher('tigrillo_w_spheres/legs_cmd', Float32MultiArray, queue_size=1)

# In a loop, send a actuation signal
t = 0
while True:
	val = [-math.pi/8 * math.sin(0.01*t), math.pi/8 * math.sin(0.01*t), \
		math.pi/8 * math.sin(0.01*t), -math.pi/8 * math.sin(0.01*t)]

	rospy.loginfo("Publishing in tigrillo/legs_cmd topic: " + str(val))
	pub.publish(Float32MultiArray(layout=MultiArrayLayout([], 1), data=val))
	
	# MWelter: time step interval was 0.001
	time.sleep(0.01)
	t += 1
	
	if t > 2000:
		exit()

# MWelter: manually		
#rostopic pub "/tigrillo/legs_cmd" std_msgs/Float32ltiArray "layout:
  #dim:
  #- label: ''
    #size: 0
    #stride: 0
  #data_offset: 0
#data: [.0, 0, 0, 0]"