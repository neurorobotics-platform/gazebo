
#include "MultithreadingTestPlugin.hh"
#include "gazebo/test/ServerFixture.hh"

namespace gazebo
{

/////////////////////////////////////////////////
std::string MultithreadingTestPlugin::custom_exec(std::string _cmd)
{
  _cmd += " 2>/dev/null";

#ifdef _WIN32
  FILE *pipe = _popen(_cmd.c_str(), "r");
#else
  FILE *pipe = popen(_cmd.c_str(), "r");
#endif

  if (!pipe)
    return "ERROR";

  char buffer[128];
  std::string result = "";

  while (!feof(pipe))
  {
    if (fgets(buffer, 128, pipe) != NULL)
      result += buffer;
  }

#ifdef _WIN32
  _pclose(pipe);
#else
  pclose(pipe);
#endif

  return result;
}


MultithreadingTestPlugin::MultithreadingTestPlugin()
  : SystemPlugin(),
    running_opensim_physics(false),
    run(true)
{
}

MultithreadingTestPlugin::~MultithreadingTestPlugin()
{
  run = false;
  spawner_thread->join();
  accessor_thread->join();
}

void MultithreadingTestPlugin::Load(int argc, char** argv)
{
  // below needs the world to be created first
  load_gazebo_ros_api_plugin_event = gazebo::event::Events::ConnectWorldCreated(boost::bind(&MultithreadingTestPlugin::OnWorldCreated,this,_1));
  sigint_event = gazebo::event::Events::ConnectSigInt(boost::bind(&MultithreadingTestPlugin::shutdownSignal,this));
}

void MultithreadingTestPlugin::Init()
{
}


void MultithreadingTestPlugin::OnWorldCreated(std::string world_name)
{
  boost::mutex::scoped_lock lock(mutex);
  if (world)
  {
    return;
  }

  world = gazebo::physics::get_world(world_name);
  if (!world)
  {
    gzerr << "cannot load multithreading test plugin, physics::get_world() fails to return world" << std::endl;
    return;
  }

#ifdef HAVE_OPENSIM
  running_opensim_physics = boost::dynamic_pointer_cast<gazebo::physics::OpensimPhysics>(
    world->Physics()) != nullptr;
#endif
  gzdbg << "MultithreadingTestPlugin running with Opensim: " << running_opensim_physics << std::endl;

  gazebonode = gazebo::transport::NodePtr(new gazebo::transport::Node());
  gazebonode->Init(world_name);

  spawner_thread.reset(new boost::thread(&MultithreadingTestPlugin::RunSpawnerThread, this));
  accessor_thread.reset(new boost::thread(&MultithreadingTestPlugin::RunAccessorThread, this));

  factory_pub = gazebonode->Advertise<gazebo::msgs::Factory>("~/factory");
}


void MultithreadingTestPlugin::SpawnModel(const std::string &_name, const ignition::math::Vector3d &pos)
{
  boost::filesystem::path path;
  path = path / TEST_PATH / "models" / _name;
  gzdbg << "Spawning ..." << path.string() << std::endl;

#if 1
  msgs::Factory msg;
  msg.set_sdf_filename("model://"+path.string());
  msg.mutable_pose()->mutable_position()->set_x(pos[0]);
  msg.mutable_pose()->mutable_position()->set_y(pos[1]);
  msg.mutable_pose()->mutable_position()->set_z(pos[2]);
  //ignition::math::Quaternion<double> q{};
  msg.mutable_pose()->mutable_orientation()->set_x(0);
  msg.mutable_pose()->mutable_orientation()->set_y(0);
  msg.mutable_pose()->mutable_orientation()->set_z(0);
  msg.mutable_pose()->mutable_orientation()->set_w(1);
  this->factory_pub->Publish(msg);
#else
  std::string cmd = "gz model -w default -m my_model -f " + path.string();
  MultithreadingTestPlugin::custom_exec(cmd);
#endif
  gzdbg << "Spawn message away!" << std::endl;
}


void MultithreadingTestPlugin::WaitTillReady()
{
  bool ready = false;
  while (!ready && run)
  {
    boost::mutex::scoped_lock lock(mutex);
    ready = world != nullptr;
  }
}


void MultithreadingTestPlugin::RunSpawnerThread()
{
  WaitTillReady();

  common::Time::MSleep(200);

  std::string model_name = running_opensim_physics ?
    "test_body_muscle" : "test_body";

  if(run)
  {
    SpawnModel(model_name, ignition::math::Vector3d(0., 0., 2.));
  }

  common::Time::MSleep(400);

  if(run)
  {
    SpawnModel(model_name, ignition::math::Vector3d(2., 0., 2.));
  }

  common::Time::MSleep(400);

  if(run)
  {
    SpawnModel(model_name, ignition::math::Vector3d(2., 2., 2.));
  }
}


enum WhatToDo : int
{
  CALL_MODEL_SETTERS = 0,
  CALL_MODEL_GETTERS = 1,
  CALL_LINK_SETTERS = 2,
  CALL_LINK_GETTERS = 3,
  CALL_JOINT_SETTERS = 4,
  CALL_JOINT_GETTERS = 5,
#ifdef HAVE_OPENSIM
  CALL_MUSCLE_SETTERS = 6,
  CALL_MUSCLE_GETTERS = 7,
#endif
  DOWHAT_COUNT,
  DOWHAT_COUNT_NO_OPENSIM = 6
};


gazebo::physics::LinkPtr MultithreadingTestPlugin::PickLink(gazebo::physics::ModelPtr model, GeneratorType &rand_generator)
{
  auto links = model->GetLinks();
  if (links.size() > 0)
  {
    int link_idx = UniformInt(rand_generator, UniformIntDist(0, links.size()-1))();
    gazebo::physics::LinkPtr link = links[link_idx];
    return link;
  }
  else
    return nullptr;
}


gazebo::physics::JointPtr MultithreadingTestPlugin::PickJoint(gazebo::physics::ModelPtr model, GeneratorType &rand_generator)
{
  auto joints = model->GetJoints();
  if (joints.size() > 0)
  {
    int idx = UniformInt(rand_generator, UniformIntDist(0, joints.size()-1))();
    gazebo::physics::JointPtr joint = joints[idx];
    return joint;
  }
  else
    return nullptr;
}


#ifdef HAVE_OPENSIM
gazebo::physics::OpensimMusclePtr MultithreadingTestPlugin::PickMuscle(gazebo::physics::OpensimModelPtr model, GeneratorType &rand_generator)
{
  auto muscles = model->GetMuscles();
  if (muscles.size() > 0)
  {
    int idx = UniformInt(rand_generator, UniformIntDist(0, muscles.size()-1))();
    gazebo::physics::OpensimMusclePtr muscle = muscles[idx];
    return muscle;
  }
  else
    return nullptr;
}
#endif


void MultithreadingTestPlugin::RunAccessorThread()
{
  WaitTillReady();

  common::Time::MSleep(200);

  GeneratorType rand_generator{42};
  UniformReal rand_double{rand_generator, boost::uniform_real<double>(-0.1, 0.1)};

  while (run)
  {
    gazebo::physics::ModelPtr model;
    {
      auto models = world->Models();
      if (models.size() > 0)
      {
        int idx = UniformInt(rand_generator, UniformIntDist(0, models.size()-1))();
        model = models[idx];
      }
    }
    if (model)
    {
      int dowhat_last_idx = (running_opensim_physics ? DOWHAT_COUNT : DOWHAT_COUNT_NO_OPENSIM) - 1;
      WhatToDo dowhat = (WhatToDo)UniformInt(rand_generator, UniformIntDist(0, dowhat_last_idx))();
      switch (dowhat)
      {
        case CALL_MODEL_SETTERS:
        {
          gzdbg << "set " << model->GetName() << "vel " << std::endl;
          ignition::math::Vector3d v(
            rand_double(),
            rand_double(),
            rand_double());
          model->SetLinearVel(v);
          model->SetAngularVel(v);
        }
        break;
        case CALL_MODEL_GETTERS:
        {
          ignition::math::Vector3d v1 = model->WorldLinearVel();
          ignition::math::Vector3d v2 = model->WorldAngularVel();
          gzdbg << "get " << model->GetName() << "vel: " << v1 << " angular vel: " << v2 << std::endl;
        }
        break;
        case CALL_LINK_SETTERS:
        {
          auto link = PickLink(model, rand_generator);
          if (link)
          {
            gzdbg << "set link " << link->GetName() << " vel." << std::endl;
            ignition::math::Vector3d v(
              rand_double(),
              rand_double(),
              rand_double());
            link->SetLinearVel(v);
            link->SetAngularVel(v);
          }
        }
        break;
        case CALL_LINK_GETTERS:
        {
          auto link = PickLink(model, rand_generator);
          if (link)
          {
            ignition::math::Vector3d v1 = link->WorldLinearVel();
            ignition::math::Vector3d v2 = link->WorldAngularVel();
            EXPECT_TRUE(v1.IsFinite());
            EXPECT_TRUE(v2.IsFinite());
            gzdbg << "get link " << link->GetName() << " vel: " << v1 << " angular vel: " << v2 << std::endl;
            ignition::math::Vector3d v3 = link->WorldAngularAccel();
            ignition::math::Vector3d v4 = link->WorldLinearAccel();
            ignition::math::Vector3d v5 = link->WorldForce();
            ignition::math::Vector3d v6 = link->WorldTorque();
            EXPECT_TRUE(v3.IsFinite());
            EXPECT_TRUE(v4.IsFinite());
            EXPECT_TRUE(v5.IsFinite());
            EXPECT_TRUE(v6.IsFinite());
          }
        }
        break;
        case CALL_JOINT_SETTERS:
        {
          auto joint = PickJoint(model, rand_generator);
          if (joint)
          {
            int axis = UniformInt(rand_generator, UniformIntDist(0, 1))();
            double val = rand_double();
            gzdbg << "set joint " << joint->GetName()  << "axis " << axis << "pos, vel = " << val << std::endl;
            joint->SetPosition(axis, val);
            joint->SetVelocity(axis, val);
          }
        }
        break;
        case CALL_JOINT_GETTERS:
        {
          auto joint = PickJoint(model, rand_generator);
          if (joint)
          {
            int axis = UniformInt(rand_generator, UniformIntDist(0, 1))();
            double v1 = joint->Position(axis); // radians
            double v2 = joint->GetVelocity(axis);
            gzdbg << "get joint " <<  joint->GetName() << "axis " << axis << " pos: " << v1 << " vel: " << v2 << std::endl;
            double v3  = joint->GetAcceleration(axis);
            EXPECT_TRUE(std::isfinite(v1));
            EXPECT_TRUE(std::isfinite(v2));
            EXPECT_TRUE(std::isfinite(v3));
          }
        }
        break;
#ifdef HAVE_OPENSIM
        case CALL_MUSCLE_SETTERS:
        {
          auto osimmodel =
            boost::static_pointer_cast<gazebo::physics::OpensimModel>(model);
          auto muscle = PickMuscle(osimmodel, rand_generator);
          if (muscle)
          {
            double val = rand_double();
            gzdbg << "set muscle " <<  muscle->GetName() << " activation: " << val << std::endl;
            muscle->SetActivationValue(val);
          }
        }
        break;
        case CALL_MUSCLE_GETTERS:
        {
          auto osimmodel =
            boost::static_pointer_cast<gazebo::physics::OpensimModel>(model);
          auto muscle = PickMuscle(osimmodel, rand_generator);
          if (muscle)
          {
            double len = muscle->GetLength();
            gzdbg << "get muscle " << muscle->GetName() << "len: " << len << std::endl;
          }
        }
        break;
#endif
        case DOWHAT_COUNT:
        default:
          break;
      }
    }
    else
      gzdbg << "Could not obtain model pointer." << std::endl;
    common::Time::MSleep(10);
  }
}


void MultithreadingTestPlugin::shutdownSignal()
{
  gzdbg << "MultithreadingTestPlugin::shutdownSignal" << std::endl;
  run = false;
}


}